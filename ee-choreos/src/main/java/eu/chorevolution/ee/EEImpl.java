/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package eu.chorevolution.ee;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;

import eu.chorevolution.chors.datamodel.Choreography;
import eu.chorevolution.chors.datamodel.ChoreographySpec;
import eu.chorevolution.chors.datamodel.xml.ChorXmlWriter;
import eu.chorevolution.exceptions.chors.ChoreographyNotFoundException;
import eu.chorevolution.exceptions.chors.DeploymentException;


public class EEImpl implements EnactmentEngine {

	private ChorRegistry reg = ChorRegistry.getInstance();

	private Logger logger = Logger.getLogger(EEImpl.class);

	@Override
	public String createChoreography(ChoreographySpec chor) {
		String chorId = reg.create(chor);
		logger.info("Choreography " + chorId + " created.");
		return chorId;
	}

	@Override
	public Choreography getChoreography(String chorId) {
		Choreography chor = reg.getChoreography(chorId);
		return chor;
	}

	@Override
	public Choreography deployChoreography(String chorId) throws DeploymentException, ChoreographyNotFoundException {
		String chorxml=null;
		String brooklyn_def=null;
		String stylesheet=null;
		CloseableHttpClient httpclient = HttpClients.createDefault();
		ByteArrayOutputStream out=new ByteArrayOutputStream();
		
		if (!reg.contains(chorId))
			throw new ChoreographyNotFoundException(chorId);
		Choreography chor = reg.getChoreography(chorId);
		ChorXmlWriter serializer = new ChorXmlWriter();
		try {
			chorxml=serializer.getChoreographyXML(chor);
			Transformer transformer = TransformerFactory.newInstance( ).newTransformer(new StreamSource(new StringReader(stylesheet)));
			transformer.transform(new StreamSource(new StringReader(chorxml)),new StreamResult(out));
			brooklyn_def=out.toString();

			HttpPost httpPost = new HttpPost("http://localhost:9200/v1/applications");
			httpPost.setEntity(new StringEntity(brooklyn_def));
			CloseableHttpResponse response2 = httpclient.execute(httpPost);
		    

			try {
			    System.out.println(response2.getStatusLine());
			    HttpEntity entity2 = response2.getEntity();
			    // do something useful with the response body
			    // and ensure it is fully consumed
			    EntityUtils.consume(entity2);
			} finally {
			    response2.close();
			}
			

		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TransformerConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TransformerFactoryConfigurationError e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TransformerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ChoreographyEnacter enacter = new ChoreographyEnacter(chor);
		return enacter.enact();
	}

	@Override
	public void updateChoreography(String chorId, ChoreographySpec spec)
			throws ChoreographyNotFoundException {
		logger.info("Requested to update choreography " + chorId);
		Choreography chor = reg.getChoreography(chorId);
		if (chor == null) {
			throw new ChoreographyNotFoundException(chorId);
		}
		if (spec.equals(chor.getChoreographySpec())) {
			logger.info("Requested to update choreography with the same spec that already have");
			return;
		}
		ChoreographyContext ctx = reg.getContext(chorId);
		ctx.setRequestedChoreographySpec(spec);
	}

}
