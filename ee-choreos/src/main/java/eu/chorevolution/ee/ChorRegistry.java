/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package eu.chorevolution.ee;

import java.io.StringReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import com.lambdaworks.redis.RedisClient;
import com.lambdaworks.redis.RedisConnection;
import com.lambdaworks.redis.RedisURI;

import eu.chorevolution.chors.datamodel.Choreography;
import eu.chorevolution.chors.datamodel.ChoreographySpec;
import eu.chorevolution.chors.datamodel.xml.ChorXmlWriter;

/**
 * Stores choreography descriptions.
 * 
 * @author leonardo
 * 
 */
public class ChorRegistry {

    private static ChorRegistry instance = new ChorRegistry();

    private AtomicInteger counter=null;
    private RedisClient redisClient=null; 
    private RedisConnection<String, String> connection=null;
    private ChorXmlWriter xml_writer=null;
    private JAXBContext chor_context=null;
	private static String chor_prefix="chor.";
    
    private ChorRegistry() {
    	String stored_id=null;
    	redisClient = new RedisClient(RedisURI.create("redis://localhost:6379"));
    	connection = redisClient.connect();
    	xml_writer=new ChorXmlWriter();
    	System.out.println("Connected to Redis");
        try {
			chor_context = JAXBContext.newInstance(Choreography.class);
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        stored_id=connection.get("counter");
        if (stored_id!=null) {
        	counter = new AtomicInteger(Integer.parseInt(stored_id));
        }
        else {
        	counter = new AtomicInteger();
        }
    }

    @Override
    protected void finalize() throws Throwable {
    	// TODO Auto-generated method stub
    	super.finalize();
	    connection.close();
	    redisClient.shutdown();
    }
    
    public static ChorRegistry getInstance() {
        return instance;
    }

    /**
     * Creates a new choreography entry
     * 
     * @return the just registred choreography ID
     */
    public String create(ChoreographySpec chorSpec) {
        String id = Integer.toString(counter.incrementAndGet());
        String xml_dump=null;
        Choreography chor = new Choreography();
        chor.setId(id);
        chor.setChoreographySpec(chorSpec);
        
        try {
			xml_dump=xml_writer.getChoreographyXML(chor);
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        connection.set(chor_prefix+id, xml_dump);
        connection.set("counter", id);
        return id;
    }
    
    public void addChoreography(Choreography chor) {
        if (connection.get(chor_prefix+chor.getId())!=null)
            throw new IllegalArgumentException("Choreography is already on registry");
		else
			try {
				connection.set(chor_prefix+chor.getId(), xml_writer.getChoreographyXML(chor));
			} catch (JAXBException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    }

    public void updateChoreography(Choreography chor) {
    	try {
    		connection.set(chor_prefix+chor.getId(), xml_writer.getChoreographyXML(chor));
    	} catch (JAXBException e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
    }

    
    public Choreography getChoreography(String chorId) {
    	String chor_xml=connection.get(chor_prefix+chorId);
    	Unmarshaller um=null;
    	Choreography result =null;
		try {
			um = chor_context.createUnmarshaller();
			result = (Choreography) um.unmarshal(new StringReader(chor_xml));
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        return result;
    }
    
    public ChoreographySpec getRequestedChoreographySpec(String chorId) {
    	String chorspec_xml=connection.get(chor_prefix+chorId);
    	Unmarshaller um=null;
    	Choreography result =null;
		try {
			um = chor_context.createUnmarshaller();
			result = (Choreography) um.unmarshal(new StringReader(chorspec_xml));
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        return result.getChoreographySpec();
    }

    public ChoreographyContext getContext(String chorId) {
    	Choreography chor = getChoreography(chorId);
        return new ChoreographyContext(chor);
    }

    public boolean contains(String chorId) {
    	if (getChoreography(chorId)!=null) 
    		return true;
    	else 
    		return false;
    }

    public Map<String, Choreography> getAll() {
        Map<String, Choreography> chors = new HashMap<String, Choreography>();
        List<String> keys = connection.keys(chor_prefix+"*");;
    	for (String key : keys) {
    		chors.put(key.replace(chor_prefix, ""), getChoreography(key));
    	}
    	
        return chors;
    }
    
    public void clean() {
    	List<String> keys = connection.keys(chor_prefix+"*");;
    	for (String key : keys) {
    		connection.del(key);
    	}
    }
}