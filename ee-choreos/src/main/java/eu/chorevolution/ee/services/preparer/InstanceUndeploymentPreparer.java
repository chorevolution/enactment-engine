package eu.chorevolution.ee.services.preparer;

import eu.chorevolution.ee.nodes.cm.NodePreparer;
import eu.chorevolution.ee.nodes.cm.NodePreparers;
import eu.chorevolution.ee.nodes.cm.NodeUpdater;
import eu.chorevolution.ee.nodes.cm.NodeUpdaters;
import eu.chorevolution.exceptions.cm.NodeNotPreparedException;
import eu.chorevolution.nodes.datamodel.CloudNode;
import eu.chorevolution.services.datamodel.DeployableService;
import eu.chorevolution.services.datamodel.ServiceInstance;

public class InstanceUndeploymentPreparer {

    private ServiceInstance instance;
    private CloudNode node;
    private DeployableService service;

    public InstanceUndeploymentPreparer(DeployableService service, ServiceInstance serviceInstance) {
	this.service = service;
	this.instance = serviceInstance;
	this.node = instance.getNode();
    }

    public void prepareUndeployment() throws PrepareUndeploymentFailedException {
	runUndeploymentPrepare();
	scheduleHandler();
    }

    private void runUndeploymentPrepare() throws PrepareUndeploymentFailedException {
	NodePreparer nodePreparer = NodePreparers.getPreparerFor(node);
	try {
	    nodePreparer.prepareNodeForUndeployment(instance.getInstanceId());
	} catch (NodeNotPreparedException e1) {
	    throw new PrepareUndeploymentFailedException();
	}
    }

    private void scheduleHandler() {
	InstanceRemoverUpdateHandler handler = new InstanceRemoverUpdateHandler(service, instance);
	NodeUpdater nodeUpdater = NodeUpdaters.getUpdaterFor(node);
	nodeUpdater.addHandler(handler);
    }
}
