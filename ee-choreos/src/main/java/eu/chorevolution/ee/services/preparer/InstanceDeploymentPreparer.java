package eu.chorevolution.ee.services.preparer;

import eu.chorevolution.ee.nodes.cm.NodePreparer;
import eu.chorevolution.ee.nodes.cm.NodePreparers;
import eu.chorevolution.ee.nodes.cm.NodeUpdater;
import eu.chorevolution.ee.nodes.cm.NodeUpdaters;
import eu.chorevolution.ee.nodes.cm.PackageTypeToCookbook;
import eu.chorevolution.ee.utils.SshWaiter;
import eu.chorevolution.exceptions.cm.NodeNotPreparedException;
import eu.chorevolution.nodes.datamodel.CloudNode;
import eu.chorevolution.services.datamodel.DeployableService;
import eu.chorevolution.services.datamodel.DeployableServiceSpec;

public class InstanceDeploymentPreparer {

    private DeployableService service;
    private CloudNode node;
    private DeployableServiceSpec newSpec;

    private String instanceId;

    SshWaiter sshWaiter = new SshWaiter();

    public InstanceDeploymentPreparer(DeployableServiceSpec newSpec, DeployableService service, CloudNode node) {
	this.newSpec = newSpec;
	this.service = service;
	this.node = node;
    }

    public void prepareDeployment() throws PrepareDeploymentFailedException {
	runDeploymentPrepare();
	scheduleHandler();
    }

    private void runDeploymentPrepare() throws PrepareDeploymentFailedException {
	NodePreparer nodePreparer = NodePreparers.getPreparerFor(node);
	String packageUri = newSpec.getPackageUri();
	String cookbookTemplateName = PackageTypeToCookbook.getCookbookName(newSpec.getPackageType());
	try {
	    instanceId = nodePreparer.prepareNodeForDeployment(packageUri, cookbookTemplateName);
	} catch (NodeNotPreparedException e1) {
	    throw new PrepareDeploymentFailedException(newSpec.getName(), node);
	}
    }

    private void scheduleHandler() {
	InstanceCreatorUpdateHandler handler = new InstanceCreatorUpdateHandler(service, instanceId, node);
	NodeUpdater nodeUpdater = NodeUpdaters.getUpdaterFor(node);
	nodeUpdater.addHandler(handler);
    }

}
