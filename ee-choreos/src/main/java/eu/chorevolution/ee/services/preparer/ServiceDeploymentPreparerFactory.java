package eu.chorevolution.ee.services.preparer;

import eu.chorevolution.services.datamodel.DeployableService;

public class ServiceDeploymentPreparerFactory {

    public static boolean testing = false;
    public static ServiceDeploymentPreparer preparerForTest;

    public static ServiceDeploymentPreparer getNewInstance(DeployableService service) {
	if (!testing)
	    return new ServiceDeploymentPreparer(service);
	else
	    return preparerForTest;
    }

}
