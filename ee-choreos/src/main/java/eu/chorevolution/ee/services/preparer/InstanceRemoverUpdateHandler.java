package eu.chorevolution.ee.services.preparer;

import eu.chorevolution.ee.nodes.cm.UpdateHandler;
import eu.chorevolution.services.datamodel.DeployableService;
import eu.chorevolution.services.datamodel.ServiceInstance;

public class InstanceRemoverUpdateHandler implements UpdateHandler {

    private ServiceInstance instance;
    private DeployableService service;

    public InstanceRemoverUpdateHandler(DeployableService service, ServiceInstance instance) {
	this.instance = instance;
	this.service = service;
    }

    @Override
    public void handle() {
	service.getSelectedNodes().remove(instance.getNode());
	service.removeInstance(instance);
    }
}
