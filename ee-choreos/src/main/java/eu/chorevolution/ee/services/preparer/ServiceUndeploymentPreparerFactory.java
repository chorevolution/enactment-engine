package eu.chorevolution.ee.services.preparer;

import eu.chorevolution.services.datamodel.DeployableService;

public class ServiceUndeploymentPreparerFactory {

    public static boolean testing = false;
    public static ServiceUndeploymentPreparer preparerForTest;

    public static ServiceUndeploymentPreparer getNewInstance(DeployableService service, int decreaseAmount) {
	if (!testing)
	    return new ServiceUndeploymentPreparer(service, decreaseAmount);
	else
	    return preparerForTest;
    }

}
