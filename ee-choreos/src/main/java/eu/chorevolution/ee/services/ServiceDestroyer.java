package eu.chorevolution.ee.services;

import eu.chorevolution.ee.services.preparer.ServiceUndeploymentPreparer;
import eu.chorevolution.ee.services.preparer.ServiceUndeploymentPreparerFactory;
import eu.chorevolution.exceptions.services.ServiceNotDeletedException;
import eu.chorevolution.exceptions.services.UpdateActionFailedException;
import eu.chorevolution.services.datamodel.DeployableService;

public class ServiceDestroyer {

    public void deleteService(DeployableService service) throws ServiceNotDeletedException {
	try {
	    this.executeUndeployment(service);
	} catch (UpdateActionFailedException e) {
	    throw new ServiceNotDeletedException(service.getUUID());
	}
    }

    private void executeUndeployment(DeployableService service) throws UpdateActionFailedException {
	ServiceUndeploymentPreparer undeploymentPreparer = ServiceUndeploymentPreparerFactory.getNewInstance(service,
		service.getServiceInstances().size());

	try {
	    undeploymentPreparer.prepareUndeployment();
	} catch (Exception e) {
	    throw new UpdateActionFailedException();
	}
    }
}