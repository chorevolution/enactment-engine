package eu.chorevolution.ee;

import java.util.ArrayList;
import java.util.List;

import eu.chorevolution.chors.datamodel.ChoreographySpec;
import eu.chorevolution.services.datamodel.LegacyService;
import eu.chorevolution.services.datamodel.LegacyServiceSpec;

public class LegacyServicesCreator {

    public List<LegacyService> createLegacyServices(ChoreographySpec chorSpec) {
        List<LegacyService> legacyServices = new ArrayList<LegacyService>();
        List<LegacyServiceSpec> specs = chorSpec.getLegacyServiceSpecs();
        if (specs != null) {
            for (LegacyServiceSpec spec : chorSpec.getLegacyServiceSpecs()) {
                LegacyService svc = new LegacyService(spec);
                legacyServices.add(svc);
            }
        }
        return legacyServices;
    }
}
