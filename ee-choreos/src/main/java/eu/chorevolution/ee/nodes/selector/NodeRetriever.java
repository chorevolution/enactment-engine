package eu.chorevolution.ee.nodes.selector;

import java.util.List;

import eu.chorevolution.ee.selectors.ObjectRetriever;
import eu.chorevolution.nodes.NodePoolManager;
import eu.chorevolution.nodes.datamodel.CloudNode;

class NodeRetriever implements ObjectRetriever<CloudNode> {

    private NodePoolManager npm;

    public NodeRetriever(NodePoolManager npm) {
        this.npm = npm;
    }

    @Override
    public List<CloudNode> retrieveObjects() {
        return this.npm.getNodes();
    }

}
