/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package eu.chorevolution.ee.nodes.cm;

import com.jcraft.jsch.JSchException;

import eu.chorevolution.ee.invoker.InvokerConfiguration;
import eu.chorevolution.ee.utils.SshCommandFailed;
import eu.chorevolution.ee.utils.SshNotConnected;
import eu.chorevolution.ee.utils.SshUtil;
import eu.chorevolution.ee.utils.SshWaiter;
import eu.chorevolution.nodes.datamodel.CloudNode;

/**
 * Considers that a node is bootstrapped if the /etc/chef folder contains the
 * files: client.pem client.rb first-boot.json validation.pem
 * 
 * @author leonardo
 * 
 */
public class BootstrapChecker {

    private SshUtil ssh = null;
    SshWaiter sshWaiter = new SshWaiter();

    public boolean isBootstrapped(CloudNode node) {

        int timeout = InvokerConfiguration.getTimeout("CONNECT_SSH");
        try {
            ssh = sshWaiter.waitSsh(node.getIp(), node.getUser(), node.getPrivateKeyFile(), timeout);
        } catch (SshNotConnected e) {
            return false;
        }

        if (!verifyChefSoloFolder())
            return false;

        return true;
    }

    private boolean verifyChefSoloFolder() {
        String result = "";
        try {
            result = ssh.runCommand("ls $HOME/chef-solo");
        } catch (JSchException e) {
            return false;
        } catch (SshCommandFailed e) {
            return false;
        }

        if (result.contains("solo.rb") && result.contains("cookbooks") && result.contains("prepare_deployment.sh")
                && result.contains("node.json") && result.contains("add_recipe_to_node.sh")) {
            return true;
        } else {
            return false;
        }
    }

}
