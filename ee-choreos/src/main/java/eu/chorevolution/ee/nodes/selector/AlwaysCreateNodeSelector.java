package eu.chorevolution.ee.nodes.selector;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import eu.chorevolution.ee.nodes.NPMFactory;
import eu.chorevolution.ee.selectors.AlwaysCreateSelector;
import eu.chorevolution.ee.selectors.NotSelectedException;
import eu.chorevolution.nodes.NodePoolManager;
import eu.chorevolution.nodes.datamodel.CloudNode;
import eu.chorevolution.services.datamodel.DeployableServiceSpec;

public class AlwaysCreateNodeSelector implements NodeSelector {

    private Map<String, AlwaysCreateSelector<CloudNode, DeployableServiceSpec>> selectors = new HashMap<String, AlwaysCreateSelector<CloudNode, DeployableServiceSpec>>();

    @Override
    public List<CloudNode> select(DeployableServiceSpec spec, int objectsQuantity) throws NotSelectedException {
	AlwaysCreateSelector<CloudNode, DeployableServiceSpec> selector = retrieveSelector(spec);
	return selector.select(spec, objectsQuantity);
    }

    private AlwaysCreateSelector<CloudNode, DeployableServiceSpec> retrieveSelector(DeployableServiceSpec spec) {
	String cloudAccount = spec.getCloudAccount();

	synchronized (this) {
	    if (!this.selectors.containsKey(cloudAccount)) {
		NodePoolManager npm = NPMFactory.getNewNPMInstance(cloudAccount);
		NodeFactory nodeFac = new NodeFactory(npm);
		this.selectors.put(cloudAccount, new AlwaysCreateSelector<CloudNode, DeployableServiceSpec>(nodeFac));
	    }
	}
	return this.selectors.get(cloudAccount);
    }
}
