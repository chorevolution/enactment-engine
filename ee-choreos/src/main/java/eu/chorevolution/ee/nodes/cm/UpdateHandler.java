package eu.chorevolution.ee.nodes.cm;

public interface UpdateHandler {

    public void handle();
    
}
