package eu.chorevolution.ee.nodes;

import eu.chorevolution.ee.config.CloudConfiguration;

public class NodeCreatorFactory {

    public static NodeCreator nodeCreatorForTesting;
    public static boolean testing;

    public NodeCreator getNewNodeCreator(CloudConfiguration cloudConfiguration) {
	if (testing) {
	    return nodeCreatorForTesting;
	} else {
	    return new NodeCreator(cloudConfiguration);
	}
    }

}
