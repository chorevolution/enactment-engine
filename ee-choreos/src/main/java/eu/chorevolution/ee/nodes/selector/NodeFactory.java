package eu.chorevolution.ee.nodes.selector;

import eu.chorevolution.ee.invoker.InvokerConfiguration;
import eu.chorevolution.ee.selectors.ObjectCreationException;
import eu.chorevolution.ee.selectors.ObjectFactory;
import eu.chorevolution.exceptions.nodes.NodeNotCreatedException;
import eu.chorevolution.nodes.NodePoolManager;
import eu.chorevolution.nodes.datamodel.CloudNode;
import eu.chorevolution.nodes.datamodel.NodeSpec;
import eu.chorevolution.services.datamodel.DeployableServiceSpec;

class NodeFactory implements ObjectFactory<CloudNode, DeployableServiceSpec> {

    private NodePoolManager npm;

    public NodeFactory(NodePoolManager npm) {
        this.npm = npm;
    }

    @Override
    public CloudNode createNewInstance(DeployableServiceSpec spec) throws ObjectCreationException {
        try {
            NodeSpec nodeSpec = new NodeSpec();
            nodeSpec.setResourceImpact(spec.getResourceImpact());
            return this.npm.createNode(nodeSpec);
        } catch (NodeNotCreatedException e) {
            throw new ObjectCreationException();
        }
    }

    @Override
    public int getTimeoutInSeconds() {
        int nodeCreationTotalTimeout = InvokerConfiguration.getTotalTimeout("NODE_CREATION");
        int firstSshTimeout = InvokerConfiguration.getTimeout("FIRST_CONNECT_SSH");
        int bootstrapTotalTimeout = InvokerConfiguration.getTotalTimeout("BOOTSTRAP");
        int oneReqPerSec = 2 * 100;
        int timeout = nodeCreationTotalTimeout + firstSshTimeout + bootstrapTotalTimeout + oneReqPerSec;
        timeout += timeout * 0.3;
        return timeout;
    }

}
