package eu.chorevolution.ee;

import java.util.List;

import org.apache.log4j.Logger;

import eu.chorevolution.chors.datamodel.Choreography;
import eu.chorevolution.chors.datamodel.ChoreographySpec;
import eu.chorevolution.ee.context.ContextCaster;
import eu.chorevolution.exceptions.chors.DeploymentException;
import eu.chorevolution.services.datamodel.DeployableService;
import eu.chorevolution.services.datamodel.LegacyService;

public class ChoreographyEnacter {

    private static Logger logger = Logger.getLogger(ChoreographyEnacter.class);

    private Choreography chor;

    private ChorRegistry reg = ChorRegistry.getInstance();

    public ChoreographyEnacter(Choreography chor) {
        this.chor = chor;
    }

    public Choreography enact() throws DeploymentException {
        logBegin();
        deploy();
        createLegacyServices();
        castContext();
        finish();
//        publish();
        logEnd();
        reg.updateChoreography(chor);
        return chor;
    }

    private void logBegin() {
        ChoreographyContext ctx = reg.getContext(chor.getId());
        ChoreographySpec requestedChoreographySpec = ctx.getRequestedChoreographySpec();
        if (chor.getChoreographySpec() == requestedChoreographySpec)
            logger.info("Starting enactment; chorId= " + chor.getId());
        else if (chor.getChoreographySpec() == requestedChoreographySpec)
            logger.info("Starting enactment for requested update; chorId= " + chor.getId());
    }

    private void deploy() throws DeploymentException {
        ServicesDeployer deployer = new ServicesDeployer(chor);
        List<DeployableService> deployedServices = deployer.deployServices();
        chor.setDeployableServices(deployedServices);
    }

    private void createLegacyServices() {
        LegacyServicesCreator legacyServicesCreator = new LegacyServicesCreator();
        List<LegacyService> legacyServices = legacyServicesCreator.createLegacyServices(chor.getChoreographySpec());
        chor.setLegacyServices(legacyServices);
    }

    private void castContext() {
        ContextCaster caster = new ContextCaster(chor);
        caster.cast();
    }

    private void finish() {
        ChoreographyContext ctx = reg.getContext(chor.getId());
        ctx.enactmentFinished();
//        ctx.startMonitoring();
    }

    private void logEnd() {
        logger.info("Enactment completed; chorId=" + chor.getId());
    }

}
