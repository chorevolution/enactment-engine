/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package eu.chorevolution.exceptions.nodes;

public class NodeNotDestroyedException extends Exception {

    private static final long serialVersionUID = -5097061342536022130L;

    public NodeNotDestroyedException(String nodeId) {
        super("Could not destroy node " + nodeId);
    }

}
