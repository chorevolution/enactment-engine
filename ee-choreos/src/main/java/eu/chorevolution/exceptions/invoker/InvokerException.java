package eu.chorevolution.exceptions.invoker;

public class InvokerException extends Exception {

    private static final long serialVersionUID = 1246428538433598611L;

    public InvokerException(Throwable cause) {
        super(cause);
    }

}