/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package eu.chorevolution.nodes;

import java.util.List;

import eu.chorevolution.exceptions.nodes.NodeNotCreatedException;
import eu.chorevolution.exceptions.nodes.NodeNotDestroyedException;
import eu.chorevolution.exceptions.nodes.NodeNotFoundException;
import eu.chorevolution.nodes.datamodel.CloudNode;
import eu.chorevolution.nodes.datamodel.NodeSpec;

public interface NodePoolManager {

    /**
     * Create a node on the cloud infrastructure.
     * 
     * @param nodeSpec
     * @return the representation of the created node
     * @throws NodeNotCreatedException
     *             if node was not created
     */
    public CloudNode createNode(NodeSpec nodeSpec) throws NodeNotCreatedException;

    /**
     * Retrieve information about all the nodes managed by this Node Pool
     * Manager
     * 
     * @return
     */
    public List<CloudNode> getNodes();

    /**
     * Retrieve information of a node according to a given node id.
     * 
     * @param nodeId
     * @return the representation of the requested node
     * @throws NodeNotFoundException
     *             if the node does not exist
     */
    public CloudNode getNode(String nodeId) throws NodeNotFoundException;

    /**
     * Destroys the Virtual Machine node
     * 
     * @param nodeId
     *            the id of the node to be destroyed
     * @throws NodeNotDestroyedException
     *             if could not destroy node
     * @throws NodeNotFoundException
     *             if the node does not exist
     */
    public void destroyNode(String nodeId) throws NodeNotDestroyedException, NodeNotFoundException;

    /**
     * Destroys all the Virtual Machine nodes
     * 
     * @throws NodeNotDestroyedException
     *             if could not destroy some node
     */
    public void destroyNodes() throws NodeNotDestroyedException;

}
