#! /bin/bash
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

COOKBOOKS_URL=$THE_COOKBOOKS_URL

function initial_setup() {
        sudo apt-get install -y python-software-properties debconf-utils
        #sudo add-apt-repository -y ppa:webupd8team/java
        #echo "oracle-java8-installer shared/accepted-oracle-license-v1-1 select true" | sudo debconf-set-selections
        #DEBIAN_FRONTEND=noninteractive sudo apt-get install -y oracle-java8-installer tomcat7
        #DEBIAN_FRONTEND=noninteractive sudo apt-get install -y oracle-java8-set-default
        sudo add-apt-repository -y ppa:openjdk-r/ppa
        sudo apt-get update
        DEBIAN_FRONTEND=noninteractive sudo apt-get install -y openjdk-8-jdk openjdk-8-jre-headless tomcat7 tomcat7-admin
		sudo service tomcat7 start
        
        if which chef-solo >/dev/null; then
            echo "Chef-solo already installed."
        else
            echo "Chef-solo not installed (going to install it)."
    #		sudo apt-get update
            curl -L https://www.opscode.com/chef/install.sh -o /tmp/install.sh
            sudo bash /tmp/install.sh -v "11.18"
        fi
}

function prepare_node() {
	if [ ! -d $HOME/chef-solo ]; then
        mkdir -p $HOME/chef-solo/cookbooks
	
		# create cookbooks repo
		cd $HOME/chef-solo
	    wget -O - $COOKBOOKS_URL | tar xzf -

		#create solo.rb file
		#cp /usr/share/chef/solo.rb .
        
        echo 'log_level          :info' > solo.rb
        echo 'log_location       STDOUT' >> solo.rb
        echo 'file_cache_path    "/var/cache/chef"' >> solo.rb
        echo 'cookbook_path      [ "/var/lib/chef/cookbooks" ]' >> solo.rb
        echo 'Mixlib::Log::Formatter.show_time = true' >> solo.rb
        
        # configuring cookbook_path in solo.rb
		chefrepo=$HOME/chef-solo/cookbooks 
		sed -i "s#^cookbook_path.*\[.*\]#cookbook_path\t\[\"$chefrepo\"\]#g" solo.rb

        # configuring json_attribs in solo.rb
		attribs=$HOME/chef-solo/node.json
		sed -i -r "s@(\#)?json_attribs.*\".*\"@json_attribs \"$attribs\"@g" solo.rb
		# remove duplicate lines (http://sed.sourceforge.net/sed1line.txt)
		# but i do not know how it works, :-)
		sed '$!N; /^\(.*\)\n\1$/!P; D' -i solo.rb

	fi
}

initial_setup > base_bootstrap.log 2>&1
prepare_node >> /tmp/chef-solo-bootstrap.log 2>&1
