/*
 * Copyright 2016 The CHOReVOLUTION project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.apache.brooklyn.rest.api;

import java.io.InputStream;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataParam;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Api("Artifacts upload")
@Path("/v1/packages")
public interface FileUploadApi {

    @POST
    @ApiOperation(
            value = "Upload a file"
    )
    @Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response uploadFile(
	        @DefaultValue("true") @FormDataParam("enabled") boolean enabled,
	        @FormDataParam("file") InputStream uploadedInputStream,
	        @ApiParam(hidden=true) @FormDataParam("file") FormDataContentDisposition fileDetail) ;
    
    
    
    @GET
    @ApiOperation(
            value = "Download a file"
    )
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response downloadFile(
            @ApiParam(value = "File name", required = true)
			@QueryParam("filename") String fileName
			) ;	
}
