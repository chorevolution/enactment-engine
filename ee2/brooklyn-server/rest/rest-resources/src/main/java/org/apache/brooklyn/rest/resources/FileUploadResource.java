/*
 * Copyright 2016 The CHOReVOLUTION project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.apache.brooklyn.rest.resources;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.apache.brooklyn.core.server.BrooklynServerPaths;
import org.apache.brooklyn.rest.api.FileUploadApi;

import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataParam;

import io.swagger.annotations.ApiParam;

public class FileUploadResource extends AbstractBrooklynRestResource implements FileUploadApi{

	private static String dir="/uploaded_content";
	/**
	 * Upon receiving file upload submission, parses the request to read
	 * upload data and saves the file on disk.
	 */
	
	public Response uploadFile(
	        @DefaultValue("true") @FormDataParam("enabled") boolean enabled,
	        @FormDataParam("file") InputStream uploadedInputStream,
	        @ApiParam(hidden=true) @FormDataParam("file") FormDataContentDisposition fileDetail) {
		if (fileDetail==null) 
			return  Response.status(400).entity("Please provide the file content").build();
		
		String fileName=fileDetail.getFileName();
		if (fileName.indexOf(File.separator)!=-1) {
			fileName=fileName.substring(fileName.indexOf(File.separator));
		}
		
	    String uploadedFileLocation = BrooklynServerPaths.getMgmtBaseDir(mgmt())+dir+"/"+ fileName;
	    File  objFile=new File(uploadedFileLocation);

	    if(objFile.exists())
	        objFile.delete();

	    saveToFile(uploadedInputStream, uploadedFileLocation);

	    return Response.status(200).entity("/packages?filename="+fileName).build();

	}
	
	public Response downloadFile(
            @ApiParam(value = "File name", required = true)
			@QueryParam("filename") String fileName
			) {
		ResponseBuilder response=null;
		if ((fileName==null)||(fileName.equals(""))) 
			return Response.status(400).entity("Filename must not be null").build();
			

		if (fileName.indexOf(File.separator)!=-1) {
			fileName=fileName.substring(fileName.indexOf(File.separator));
		}
		
		String fileLocation = BrooklynServerPaths.getMgmtBaseDir(mgmt())+dir+"/"+ fileName;
		File file = new File(fileLocation);

		response = Response.ok((Object) file);
		response.header("Content-Disposition",
				"attachment; filename=\""+fileName+"\"");
		return response.build();
		
	}
	
	
	private void saveToFile(InputStream uploadedInputStream,
	        String uploadedFileLocation) {

	    try {
	        OutputStream out = null;
	        int read = 0;
	        byte[] bytes = new byte[1024];

	        out = new FileOutputStream(new File(uploadedFileLocation));
	        while ((read = uploadedInputStream.read(bytes)) != -1) {
	            out.write(bytes, 0, read);
	        }
	        out.flush();
	        out.close();
	    } catch (IOException e) {

	        e.printStackTrace();
	    }

	}
	


}
