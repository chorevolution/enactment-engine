/*
 * Copyright 2016 The CHOReVOLUTION project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.apache.brooklyn.rest.resources;

import static javax.ws.rs.core.Response.status;
import static javax.ws.rs.core.Response.Status.ACCEPTED;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.StringReader;
import java.util.Collection;

import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.brooklyn.api.entity.Application;
import org.apache.brooklyn.api.entity.Entity;
import org.apache.brooklyn.api.entity.EntitySpec;
import org.apache.brooklyn.api.location.Location;
import org.apache.brooklyn.api.typereg.RegisteredType;
import org.apache.brooklyn.core.entity.lifecycle.ServiceStateLogic;
import org.apache.brooklyn.core.mgmt.entitlement.Entitlements;
import org.apache.brooklyn.core.mgmt.entitlement.Entitlements.StringAndArgument;
import org.apache.brooklyn.core.typereg.RegisteredTypeLoadingContexts;
import org.apache.brooklyn.core.typereg.RegisteredTypePredicates;
import org.apache.brooklyn.entity.webapp.ControlledDynamicWebAppCluster;
import org.apache.brooklyn.entity.webapp.chorevolution.Choreography;
import org.apache.brooklyn.entity.webapp.chorevolution.ControlledDynamicChoreographyCluster;
import org.apache.brooklyn.rest.api.ChoreographyApi;
import org.apache.brooklyn.rest.util.WebResourceUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Lists;

import eu.chorevolution.datamodel.ServiceGroup;

public class ChoreographyResource extends AbstractBrooklynRestResource implements ChoreographyApi {

    private static final Logger log = LoggerFactory.getLogger(ChoreographyResource.class);

    @Override
    public Response deploy(String choreography_name, InputStream chorSpec) {
        String brooklyn_def = null;
        EntitySpec chor=null;
        ApplicationResource ar=new ApplicationResource();
        log.info("Deploying choreography "+choreography_name+" | specifications: {}", chorSpec);
        ByteArrayOutputStream out = new ByteArrayOutputStream();
       
        try {
            InputStream xslt_content = this.getClass().getClassLoader().getSystemResourceAsStream("chor2brooklyn.xslt");
            Transformer transformer = TransformerFactory.newInstance().newTransformer(new StreamSource(xslt_content));
            transformer.transform(new StreamSource(chorSpec), new StreamResult(out));
            brooklyn_def = out.toString().replace("CHOREOGRAPHY_NAME", choreography_name).replaceAll("\t","    ");
            
            log.info("Created YAML specs: "+brooklyn_def);
            ar.createFromYaml(brooklyn_def);
            
            chor = mgmt().getTypeRegistry().createSpecFromPlan(null, brooklyn_def, RegisteredTypeLoadingContexts.spec(Choreography.class), EntitySpec.class);
            return ar.launch(brooklyn_def, chor);

        } catch (TransformerConfigurationException ie) {
            // TODO Auto-generated catch block
            ie.printStackTrace();
        } catch (TransformerFactoryConfigurationError ie) {
            // TODO Auto-generated catch block
            ie.printStackTrace();
        } catch (TransformerException ie) {
            // TODO Auto-generated catch block
            ie.printStackTrace();
        } 

        return null;
    }
    
    @Override
    public Response update(String choreographyId, String choreographyName, InputStream chorSpec) {
        // TODO Auto-generated method stub
        return Response.accepted().build();
    }
    
    @Override
    public Response undeploy(String choreography_id) {
        // TODO Auto-generated method stub
        return Response.accepted().build();
    }

    @Override
    public Response checkStatus(String choreography_id) {
        // TODO Auto-generated method stub
        for (Application a:mgmt().getApplications()) {
            if (a.getApplicationId().equals(choreography_id)) {
                ServiceStateLogic.getExpectedState(a);

            }
        }
        return null;
    }

    @Override
    public Response start(String choreography_id) {
    	String symbolicName=choreography_id;
    	if (!Entitlements.isEntitled(mgmt().getEntitlementManager(), Entitlements.MODIFY_CATALOG_ITEM, StringAndArgument.of(symbolicName, "delete"))) {
    		throw WebResourceUtils.forbidden("User '%s' is not authorized to modify catalog",
    				Entitlements.getEntitlementContext().user());
    	}

    	Application chor_app = brooklyn().getApplication(choreography_id);
    	for (Entity c:chor_app.getChildren()) {
    		if (c instanceof ControlledDynamicChoreographyCluster) {
    			ControlledDynamicChoreographyCluster chor= (ControlledDynamicChoreographyCluster)c;
    	        Collection<Location> locations = Lists.newArrayList(chor.getLocations());
    	        chor.start(locations);
    		}
    	}

    	return null;
    }

    @Override
    public Response stop(String choreography_id) {
    	String symbolicName=choreography_id;
        // TODO Auto-generated method stub
    	
    	if (!Entitlements.isEntitled(mgmt().getEntitlementManager(), Entitlements.MODIFY_CATALOG_ITEM, StringAndArgument.of(symbolicName, "delete"))) {
            throw WebResourceUtils.forbidden("User '%s' is not authorized to modify catalog",
                Entitlements.getEntitlementContext().user());
        }
        
        RegisteredType item = mgmt().getTypeRegistry().get(symbolicName);
        if (item == null) {
            throw WebResourceUtils.notFound("Entity with id '%s' not found", symbolicName);
        } else if (!RegisteredTypePredicates.IS_ENTITY.apply(item) && !RegisteredTypePredicates.IS_APPLICATION.apply(item)) {
            throw WebResourceUtils.preconditionFailed("Item with id '%s' not an entity", symbolicName);
        } else {
            brooklyn().getCatalog().deleteCatalogItem(item.getSymbolicName(), item.getVersion());
        }
        return null;
    }

    @Override
    public Response pause(String choreography_id) {
    	String symbolicName=choreography_id;
        // TODO Auto-generated method stub
    	
    	if (!Entitlements.isEntitled(mgmt().getEntitlementManager(), Entitlements.MODIFY_CATALOG_ITEM, StringAndArgument.of(symbolicName, "delete"))) {
            throw WebResourceUtils.forbidden("User '%s' is not authorized to modify catalog",
                Entitlements.getEntitlementContext().user());
        }
        
        Application chor_app = brooklyn().getApplication(choreography_id);
        for (Entity c:chor_app.getChildren()) {
        	if (c instanceof ControlledDynamicChoreographyCluster) {
        		ControlledDynamicChoreographyCluster chor= (ControlledDynamicChoreographyCluster)c;
        		((ControlledDynamicWebAppCluster)chor).stop();
        	}
        }
        // TODO Auto-generated method stub
        return null;
    }

	@Override
	public Response resize(String choreography_name, Integer desired_size) {
		String symbolicName=choreography_name;
        // TODO Auto-generated method stub
    	
    	if (!Entitlements.isEntitled(mgmt().getEntitlementManager(), Entitlements.MODIFY_CATALOG_ITEM, StringAndArgument.of(symbolicName, "delete"))) {
            throw WebResourceUtils.forbidden("User '%s' is not authorized to modify catalog",
                Entitlements.getEntitlementContext().user());
        }
        
        Application chor_app = brooklyn().getApplication(choreography_name);
        for (Entity c:chor_app.getChildren()) {
        	if (c instanceof ControlledDynamicChoreographyCluster) {
        		ControlledDynamicChoreographyCluster chor= (ControlledDynamicChoreographyCluster)c;
        		return status(ACCEPTED).entity(chor.resize(desired_size)).build();
        	}
        }
        // TODO Auto-generated method stub
        return null;

	}

    @Override
    public Response replaceService(
            String choreographyId, String serviceRole, String serviceName, String serviceEndpoint) {

        // TODO Auto-generated method stub
        return null;
    }	        
        
	private ServiceGroup create_new(String xml) throws JAXBException{
        JAXBContext context = JAXBContext.newInstance(ServiceGroup.class);
        Unmarshaller unmarshaller = context.createUnmarshaller();
        ServiceGroup c = (ServiceGroup) unmarshaller.unmarshal(new StringReader(xml));

        return c;
    }	

}
