<!--
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 
-->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<xsl:output method="text" omit-xml-declaration="yes"/>
	<xsl:template match="/choreography">
name: CHOREOGRAPHY_NAME

location: named:cefriel-openstack

services:
    <xsl:apply-templates></xsl:apply-templates>
</xsl:template>

<xsl:template match="service_groups">
- type: org.apache.brooklyn.entity.webapp.chorevolution.ControlledDynamicChoreographyCluster
  name: StApp_wp4_<xsl:number/>
  brooklyn.config:
    chorspec: |<xsl:for-each select="services">
      - service:
          name: <xsl:value-of select="name"/> 
          <xsl:if test="roles">
          roles: <xsl:value-of select="roles"/>
          </xsl:if>
          <xsl:if test="serviceType">
          service_type: <xsl:value-of select="serviceType"/>
          </xsl:if>
          <xsl:if test="@xsi:type">
          artifact_type: <xsl:value-of select="@xsi:type"/>
          </xsl:if>
          <xsl:if test="packageType">
          package_type: <xsl:value-of select="packageType"/>
          </xsl:if>
          <xsl:if test="packageUrl">
          package_url: <xsl:value-of select="packageUrl"/>
          </xsl:if>
          <xsl:if test="dependencies">
          dependencies:
            <xsl:for-each select="dependencies">
                <xsl:text>- </xsl:text><xsl:value-of select="serviceSpecRole" /><xsl:text>: </xsl:text><xsl:value-of select="serviceSpecName" />
                <xsl:choose>
                    <xsl:when test="position() != last()">
                        <xsl:text>&#xa;            </xsl:text>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:text>&#xa;      </xsl:text>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:for-each>
          </xsl:if>
      </xsl:for-each>

          
    wars.named: [<xsl:for-each select="services[packageType='WAR']">
      "<xsl:value-of select="packageUrl"/>"<xsl:choose><xsl:when test="position() != last()">,</xsl:when></xsl:choose>
    </xsl:for-each>
    ]
    coordination_delegates:
    <xsl:for-each select="services[packageType='ODE']">
      <xsl:text>  </xsl:text><xsl:value-of select="name"/><xsl:text>: '</xsl:text><xsl:value-of select="packageUrl"/>'
    </xsl:for-each>

    java.version.required: 1.8
    memberSpec:
      $brooklyn:entitySpec:
        type: brooklyn.entity.webapp.tomcat.Tomcat8Server
        install.version: 8.0.22
        http.port: 8080
        webapp.enabledProtocols: [http, https]
        java.opts: [ "-Djava.security.egd=file:/dev/./urandom", "-Djavax.xml.accessExternalSchema=http"]
        java.sysprops:
          file.encoding: UTF-8
</xsl:template>

</xsl:stylesheet>
