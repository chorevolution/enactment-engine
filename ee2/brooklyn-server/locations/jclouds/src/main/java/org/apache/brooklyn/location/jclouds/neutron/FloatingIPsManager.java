/*
 * CEFRIEL
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package org.apache.brooklyn.location.jclouds.neutron;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Iterator;
import java.util.Set;

import org.jclouds.ContextBuilder;
import org.jclouds.collect.IterableWithMarker;
import org.jclouds.collect.PagedIterable;
import org.jclouds.logging.slf4j.config.SLF4JLoggingModule;
import org.jclouds.openstack.neutron.v2.NeutronApi;
import org.jclouds.openstack.neutron.v2.domain.FloatingIP;
import org.jclouds.openstack.neutron.v2.domain.IP;
import org.jclouds.openstack.neutron.v2.domain.Port;
import org.jclouds.openstack.neutron.v2.extensions.FloatingIPApi;
import org.jclouds.openstack.neutron.v2.features.PortApi;
import org.jclouds.openstack.nova.v2_0.NovaApi;
import org.jclouds.openstack.nova.v2_0.domain.Server;
import org.jclouds.openstack.nova.v2_0.features.ServerApi;

import com.google.common.base.Optional;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.ImmutableSet;
import com.google.common.io.Closeables;
import com.google.inject.Module;

public class FloatingIPsManager {

    // API providers' names
    private static String novaProvider = "openstack-nova";
    private static String neutronProvider = "openstack-neutron";

    private NovaApi novaApi;
    private NeutronApi neutronApi;
    private String regionID;

    // Default OpenStack's public network ID
    private String floatingNetworkID = "7f390079-4eb7-4e93-a72d-d957846c8846";

    public static void main(String[] args) {
        System.out.println("\n\nThis is a Java utility that uses JClouds 1.9.2 to assign a floating IP to a VM.");
        System.out.println(
                "This console program is made just for testing purposes, we suggest to include the FloatingIPsManager java class into your on development project and call the static method allocateFloatingIPToServer\n\n");
        System.out.println("\nPlease insert the OpenStack's server (VM) ID");
        try {
            BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
            String serverID = bufferRead.readLine();

            System.out.println(
                    "\nPlease insert the OpenStack's public network ID. Leave it blank in case you want to use the default one (related to CEFRIEL's infrastructure)");
            String networkID = bufferRead.readLine();

            System.out.println("\nA new floating IP is being allocated and assigned to the server...");
            FloatingIPsManager ipman = new FloatingIPsManager("chorevolution:enactment_engine", "chorevolution", "http://172.16.150.2:5000/v2.0/", networkID);
            FloatingIP newIP = ipman.allocateFloatingIPToServer(serverID, networkID);
            if (newIP == null)
                System.out.println("An error has occurred during the process...");
            else
                System.out.println("\n\nThe IP Address " + newIP.getFloatingIpAddress()
                        + " has been allocated and assigned to the fixed IP Address " + newIP.getFixedIpAddress());
            System.out.println("Thank you. Bye");
            ipman.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Constructor. It creates the connections to the OpenStack APIs and set the
     * class variables
     * 
     * @param floatingNetworkID
     *            The OpenStack's ID of the public network, where the floating
     *            IP will be taken. Pass a NULL or empty string if you want to
     *            use the default value wired into the code
     */
    public FloatingIPsManager(String identity, String credential, String endpoint, String floatingNetworkID) {
        this.floatingNetworkID = floatingNetworkID;
        Iterable<Module> modules = ImmutableSet.<Module> of(new SLF4JLoggingModule());

        // Get the reference to our OpenStack infrastructure's Nova API
        novaApi = ContextBuilder.newBuilder(novaProvider).endpoint(endpoint).credentials(identity, credential)
                .modules(modules).buildApi(NovaApi.class);
        Set<String> regions = novaApi.getConfiguredRegions();

        // Get the reference to our OpenStack infrastructure's Neutron API
        neutronApi = ContextBuilder.newBuilder(neutronProvider).endpoint(endpoint).credentials(identity, credential)
                .modules(modules).buildApi(NeutronApi.class);

        // Since there is only one region in our OpenStack infrastructure, I
        // take the first one in the regions collection
        regionID = regions.iterator().next();

        // If a different floatingNetworkID has been set, use that one instead
        // of the default one
        if (!(floatingNetworkID == null || floatingNetworkID.equals("")))
            this.floatingNetworkID = floatingNetworkID;

    }



    /**
     * 
     * This method does all the operations needed to allocate a floating IP with
     * Neutron and associate it to the server (VM) specified in the parameter.
     * Please not that there are a couple of prerequisites to take into account:
     * - It is assumed that there is only one Region in the OpenStack
     * infrastructure - It is assumed that the server (VM) that will receive the
     * floating IP has ONLY ONE network interface already configured by
     * OpenStack with a fixed IP address - It is assumed that the IP address
     * configured on the server (VM) has been used ONLY ONCE inside the
     * OpenStack tenant (project) we are using.
     * 
     * @param serverID
     *            The OpenStack's ID of the server (VM) that will receive a
     *            floating IP
     * @param floatingNetworkID
     *            The OpenStack's ID of the public network, where the floating
     *            IP will be taken. Pass a NULL or empty string if you want to
     *            use the default value wired into the code
     * @return The Allocated floating IP in the JClouds defined data structure.
     * @throws Exception
     *             An error has occured in the process. Have a look at the stack
     *             trace to debug it.
     */
    public FloatingIP allocateFloatingIPToServer(String serverID, String floatingNetworkID) throws Exception  {
        // Create a new instance of the Class and prepare the connections to the
        // OpenStack APIs
        Server myserver = findServer(serverID);
        if (myserver==null) {
            System.out.println("No server found for id "+serverID);
        	return null;
        }
        
        String address = findAddress(myserver);
        if (address==null) {
        	System.out.println("No ip  addresses found for id "+serverID);
        	return null;
        }
        
        String portID = findPortID(address);
        FloatingIP allocatedFloatingIP = allocateFloatingIP(address, portID);
        // Close all the connections to the OpenStack APIs
        return allocatedFloatingIP;
    }


    public String findAddress(String serverID) throws Exception {
        Server s=findServer(serverID);
        if (s!=null) {
            return findAddress(s);
        }
        else return null;
    }

    
    public String findFloatingIpAddress(String serverID) throws Exception {
    	String fixedAddr=null;
        Server s=findServer(serverID);
        
        if (s==null) 
        	return null;
        
        fixedAddr=findAddress(s);
        Optional<? extends FloatingIPApi> apiOption = neutronApi.getFloatingIPApi(regionID);
        FloatingIPApi api = apiOption.get();
        PagedIterable<FloatingIP> paged_addresses = api.list();
        
        FluentIterable<FloatingIP> addresses = paged_addresses.concat();
		for (FloatingIP addr: addresses) {
        	if (fixedAddr.equals(addr.getFixedIpAddress())) {
        		return addr.getFloatingIpAddress();
        	}
        }
		return null;
    }
    
    /**
     * Given a serverID, the function retrieve all the server information, and
     * return them into the JClouds defined data structure
     * 
     * @param serverID
     * @return a JClouds server object
     * @throws Exception
     */
    private Server findServer(String serverID) throws Exception {
        ServerApi serverApi = novaApi.getServerApi(regionID);
        return serverApi.get(serverID);
    }

    /**
     * Given a JClouds Server object, this function retrieve the first IP
     * address configured on it. *ATTENTION*: We are giving for granted that the
     * server passed as a parameter has ONLY ONE network interface with a fixed
     * IP address configured on it
     * 
     * @param server
     * @return
     * @throws Exception
     */
    private String findAddress(Server server) throws Exception {
        // The next instruction recovers the first IP address present in the
        // "Server" JClouds data structure. In this data structure, IP
        // addresses are contained into a Multimap<String, Address> collection.
        // The key we use to recover the IP address is "internal", and this key
        // has been found through tests with JClouds. We should get a
        // Collection of "Address", but since we are talking about VMs just
        // created with only one network interface, the first address of this
        // list is the one we need
        return server.getAddresses().get("internal").iterator().next().getAddr();
    }

    /**
     * This function looks for the Neutron port (for the current OpenStack
     * tenant) that has the IP address passed as parameter configured on it.
     * *ATTENTION*: We are giving for granted that the IP address passed as
     * parameters has been used ONLY ONCE inside this OpenStack's tenants. If
     * this is not true, this function would need to be modified in order to
     * identify the correct port.
     * 
     * @param address
     * @return
     * @throws Exception
     */
    private String findPortID(String address) throws Exception {
        PortApi portApi = neutronApi.getPortApi(regionID);
        for (Port port : portApi.list().concat()) {
            for (IP ip : port.getFixedIps()) {
                if (ip.getIpAddress().equals(address))
                    return port.getId();
            }
        }
        return null;
    }

	/**
	 * This method assign a floating IP to the to the neutron port and to the
	 * fixed IP address identified previously.
	 * 
	 * The floating IP is either recovered from a previously allocated but
	 * unused one, or created brand new.
	 * 
	 * 
	 * @param fixedAddress
	 * @param portID
	 * @return
	 * @throws Exception
	 */
	private FloatingIP allocateFloatingIP(String fixedAddress, String portID) throws Exception {

		Optional<? extends FloatingIPApi> apiOption = neutronApi.getFloatingIPApi(regionID);

		FloatingIPApi api = apiOption.get();

		FloatingIP unusedIP = findUnusedIP(api);
		if (unusedIP == null) {
			// The following instructions create a floating IP taken from the
			// network indicated in "floatingNetworkID", and assign it to the
			// fixed
			// IPv4 address and to the portID set in the parameters
			FloatingIP.CreateFloatingIP builder = FloatingIP.createBuilder(floatingNetworkID)
					.fixedIpAddress(fixedAddress).portId(portID).build();
			return api.create(builder);
		} else {
			FloatingIP.UpdateFloatingIP updatedIP = FloatingIP.updateBuilder().fixedIpAddress(fixedAddress)
					.portId(portID).build();

			return api.update(unusedIP.getId(), updatedIP);
		}
	}
    
	/**
	 * This method look for a floating IP already allocated but no longer in use
	 * by any server
	 * 
	 * @param api
	 *            the jClouds FloatingIP API
	 * @return An unused Floating IP, if any; null otherwise
	 */
	private FloatingIP findUnusedIP(FloatingIPApi api) {
		PagedIterable<FloatingIP> floatingIPsList = api.list();

		Iterator<IterableWithMarker<FloatingIP>> iterator = floatingIPsList.iterator();
		while (iterator.hasNext()) {
			IterableWithMarker<FloatingIP> it = iterator.next();
			for (int i = 0; i < it.size(); i++) {
				FloatingIP ip = it.get(i);
				if (ip.getFixedIpAddress() == null && ip.getFloatingNetworkId().equals(floatingNetworkID))
					return ip;
			}
		}
		return null;
	}
    
    /**
     * This function closes the connection to the OpenStack's APIs
     * 
     * @throws IOException
     */
    public void close() throws IOException {
        Closeables.close(novaApi, true);
        Closeables.close(neutronApi, true);
    }
}
