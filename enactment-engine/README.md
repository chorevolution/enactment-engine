CHOReVOLUTION Enactment Engine
===

This repository contains the CHOReVOLUTION Enactment Engine. The Enactment Engine adds choreographies to Apache Brooklyn and is distributed as an OSGi bundle.

### Building the Enactment Engine

To build an assembly, simply run:

    cd enactment-engine
    mvn clean package

This creates an OSGi bundle at:

    target/ee2-0.1.0-SNAPSHOT.jar

### Installing the Enactment Engine

To install the Enactment Engine, download the Apache Brooklyn binary from https://brooklyn.apache.org/download/index.html
Once installed/unpacked, place the Enactment Engine OSGi jar bundle inside

    ee-install-dir/lib/patch

### Opening in an IDE

To open this project in an IDE, you will need maven support enabled
(e.g. with the relevant plugin).  You should then be able to develop
it and run it as usual.  For more information on IDE support, visit:

    https://brooklyn.apache.org/v/latest/dev/env/ide/




### More About Apache Brooklyn

The CHOReVOLUTION Enactment Engine is implemented on top of Apache Brooklyn.
Apache Brooklyn is a code library and framework for managing applications in a 
cloud-first dev-ops-y way.  

For more information consider visiting the Apache Brooklyn home page at 

    https://brooklyn.apache.org


### License

Licensed to the Apache Software Foundation (ASF) under one or more contributor license agreements. See the NOTICE file distributed with this work for additional information regarding copyright ownership. The ASF licenses this file to you under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
