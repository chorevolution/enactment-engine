/*
 * Copyright 2016 The CHOReVOLUTION project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.apache.brooklyn.rest.resources;

import static javax.ws.rs.core.Response.created;
import static javax.ws.rs.core.Response.status;
import static javax.ws.rs.core.Response.Status.ACCEPTED;
import static javax.ws.rs.core.Response.Status.INTERNAL_SERVER_ERROR;
import static org.apache.brooklyn.rest.util.WebResourceUtils.serviceAbsoluteUriBuilder;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.StringReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collection;
import java.util.concurrent.Callable;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.brooklyn.api.entity.Application;
import org.apache.brooklyn.api.entity.Entity;
import org.apache.brooklyn.api.entity.EntitySpec;
import org.apache.brooklyn.api.location.Location;
import org.apache.brooklyn.api.typereg.RegisteredType;
import org.apache.brooklyn.core.config.ConstraintViolationException;
import org.apache.brooklyn.core.entity.Attributes;
import org.apache.brooklyn.core.entity.Entities;
import org.apache.brooklyn.core.entity.lifecycle.Lifecycle;
import org.apache.brooklyn.core.entity.lifecycle.ServiceStateLogic;
import org.apache.brooklyn.core.entity.trait.Resizable;
import org.apache.brooklyn.core.entity.trait.Startable;
import org.apache.brooklyn.core.mgmt.EntityManagementUtils;
import org.apache.brooklyn.core.mgmt.EntityManagementUtils.CreationResult;
import org.apache.brooklyn.core.mgmt.entitlement.Entitlements;
import org.apache.brooklyn.core.mgmt.entitlement.Entitlements.EntityAndItem;
import org.apache.brooklyn.core.mgmt.entitlement.Entitlements.StringAndArgument;
import org.apache.brooklyn.core.typereg.RegisteredTypePredicates;
import org.apache.brooklyn.entity.webapp.ControlledDynamicWebAppCluster;
import org.apache.brooklyn.rest.api.ApplicationApi;
import org.apache.brooklyn.rest.api.ChoreographyApi;
import org.apache.brooklyn.rest.transform.TaskTransformer;
import org.apache.brooklyn.rest.util.WebResourceUtils;
import org.apache.brooklyn.util.core.ResourceUtils;
import org.apache.brooklyn.util.exceptions.Exceptions;
import org.apache.brooklyn.util.exceptions.UserFacingException;
import org.apache.brooklyn.util.net.Urls;
import org.apache.brooklyn.util.repeat.Repeater;
import org.apache.brooklyn.util.time.Duration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

import eu.chorevolution.datamodel.ServiceGroup;
import eu.chorevolution.enactment.Choreography;
import eu.chorevolution.enactment.entity.ControlledDynamicChoreographyCluster;

public class ChoreographyResource extends AbstractBrooklynRestResource implements ChoreographyApi {

    private static final Logger log = LoggerFactory.getLogger(ChoreographyResource.class);

    @Override
    public Response deploy(String choreography_name, InputStream chorSpec) {
        String brooklyn_def = null;
        ApplicationResource ar=new ApplicationResource();
        log.info("Deploying choreography "+choreography_name+" | specifications: {}", chorSpec);
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        try {
            InputStream xslt_content = this.getClass().getClassLoader().getSystemResourceAsStream("chor2brooklyn.xslt");
            Transformer transformer = TransformerFactory.newInstance().newTransformer(new StreamSource(xslt_content));
            transformer.transform(new StreamSource(chorSpec), new StreamResult(out));
            brooklyn_def = out.toString().replace("CHOREOGRAPHY_NAME", choreography_name).replaceAll("\t","    ");

            log.info("Created YAML specs: "+brooklyn_def);
            return createFromYaml(brooklyn_def);

        } catch (TransformerConfigurationException ie) {
            // TODO Auto-generated catch block
            ie.printStackTrace();
        } catch (TransformerFactoryConfigurationError ie) {
            // TODO Auto-generated catch block
            ie.printStackTrace();
        } catch (TransformerException ie) {
            // TODO Auto-generated catch block
            ie.printStackTrace();
        } 

        return null;
    }

    @Override
    public Response update(String choreographyId, String choreographyName, InputStream chorSpec) {
        // TODO Auto-generated method stub
        return Response.accepted().build();
    }

    @Override
    public Response undeploy(String choreography_id) {
        // TODO Auto-generated method stub
        return Response.accepted().build();
    }

    @Override
    public Response checkStatus(String choreography_id) {
        // TODO Auto-generated method stub
        for (Application a:mgmt().getApplications()) {
            if (a.getApplicationId().equals(choreography_id)) {
                ServiceStateLogic.getExpectedState(a);

            }
        }
        return null;
    }

    @Override
    public Response start(String choreography_id) {
        String symbolicName=choreography_id;
        if (!Entitlements.isEntitled(mgmt().getEntitlementManager(), Entitlements.MODIFY_CATALOG_ITEM, StringAndArgument.of(symbolicName, "delete"))) {
            throw WebResourceUtils.forbidden("User '%s' is not authorized to modify catalog",
                    Entitlements.getEntitlementContext().user());
        }

        Application chor_app = brooklyn().getApplication(choreography_id);
        for (Entity c:chor_app.getChildren()) {
            if (c instanceof ControlledDynamicChoreographyCluster) {
                ControlledDynamicChoreographyCluster chor= (ControlledDynamicChoreographyCluster)c;
                Collection<Location> locations = Lists.newArrayList(chor.getLocations());
                chor.start(locations);
            }
        }

        return null;
    }

    @Override
    public Response stop(String choreography_id) {
        String symbolicName=choreography_id;
        // TODO Auto-generated method stub

        if (!Entitlements.isEntitled(mgmt().getEntitlementManager(), Entitlements.MODIFY_CATALOG_ITEM, StringAndArgument.of(symbolicName, "delete"))) {
            throw WebResourceUtils.forbidden("User '%s' is not authorized to modify catalog",
                    Entitlements.getEntitlementContext().user());
        }

        RegisteredType item = mgmt().getTypeRegistry().get(symbolicName);
        if (item == null) {
            throw WebResourceUtils.notFound("Entity with id '%s' not found", symbolicName);
        } else if (!RegisteredTypePredicates.IS_ENTITY.apply(item) && !RegisteredTypePredicates.IS_APPLICATION.apply(item)) {
            throw WebResourceUtils.preconditionFailed("Item with id '%s' not an entity", symbolicName);
        } else {
            brooklyn().getCatalog().deleteCatalogItem(item.getSymbolicName(), item.getVersion());
        }
        return null;
    }

    @Override
    public Response pause(String choreography_id) {
        String symbolicName=choreography_id;
        // TODO Auto-generated method stub

        if (!Entitlements.isEntitled(mgmt().getEntitlementManager(), Entitlements.MODIFY_CATALOG_ITEM, StringAndArgument.of(symbolicName, "delete"))) {
            throw WebResourceUtils.forbidden("User '%s' is not authorized to modify catalog",
                    Entitlements.getEntitlementContext().user());
        }

        Application chor_app = brooklyn().getApplication(choreography_id);
        for (Entity c:chor_app.getChildren()) {
            if (c instanceof ControlledDynamicChoreographyCluster) {
                ControlledDynamicChoreographyCluster chor= (ControlledDynamicChoreographyCluster)c;
                ((ControlledDynamicWebAppCluster)chor).stop();
            }
        }
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Response resize(String choreography_name, Integer desired_size) {
        if (!Entitlements.isEntitled(mgmt().getEntitlementManager(), Entitlements.MODIFY_CATALOG_ITEM, StringAndArgument.of(choreography_name, "delete"))) {
            throw WebResourceUtils.forbidden("User '%s' is not authorized to modify catalog",
                    Entitlements.getEntitlementContext().user());
        }
        try {
            Application chor_app = brooklyn().getApplication(choreography_name);
            for (Entity c:chor_app.getChildren()) {
                if (c instanceof Choreography) {
                    Iterable<ControlledDynamicChoreographyCluster> targets = Iterables.filter(c.getChildren(), ControlledDynamicChoreographyCluster.class);
                    Entities.invokeEffectorList(c, targets, Resizable.RESIZE, ImmutableMap.of("desiredSize", desired_size));
                }
            }
            return status(ACCEPTED).entity(desired_size).build();
        } catch (Exception e) {
            return status(INTERNAL_SERVER_ERROR).entity(stackTraceToString(e)).build();
        }
    }

    @Override
    public Response replaceService(
            String choreographyId, String serviceRole, String serviceName, String serviceEndpoint) {

        // TODO Auto-generated method stub
        return null;
    }	        

    private ServiceGroup create_new(String xml) throws JAXBException{
        JAXBContext context = JAXBContext.newInstance(ServiceGroup.class);
        Unmarshaller unmarshaller = context.createUnmarshaller();
        ServiceGroup c = (ServiceGroup) unmarshaller.unmarshal(new StringReader(xml));

        return c;
    }	


    public Response createFromYaml(String yaml) {
        // First of all, see if it's a URL
        Preconditions.checkNotNull(yaml, "Blueprint must not be null");
        URI uri = null;
        try {
            String yamlUrl = yaml.trim();
            if (Urls.isUrlWithProtocol(yamlUrl)) {
                uri = new URI(yamlUrl);
            }
        } catch (URISyntaxException e) {
            // It's not a URI then...
            uri = null;
        }
        if (uri != null) {
            log.debug("Create app called with URI; retrieving contents: {}", uri);
            try {
                yaml = ResourceUtils.create(mgmt()).getResourceAsString(uri.toString());
            } catch (Exception e) {
                Exceptions.propagateIfFatal(e);
                throw new UserFacingException("Cannot resolve URL: "+uri, e);
            }
        }

        log.debug("Creating app from yaml:\n{}", yaml);

        EntitySpec<? extends Application> spec;
        try {
            spec = createEntitySpecForApplication(yaml);
        } catch (Exception e) {
            Exceptions.propagateIfFatal(e);
            UserFacingException userFacing = Exceptions.getFirstThrowableOfType(e, UserFacingException.class);
            if (userFacing!=null) {
                log.debug("Throwing "+userFacing+", wrapped in "+e);
                throw userFacing;
            }
            throw WebResourceUtils.badRequest(e, "Error in blueprint");
        }

        if (!Entitlements.isEntitled(mgmt().getEntitlementManager(), Entitlements.DEPLOY_APPLICATION, spec)) {
            throw WebResourceUtils.forbidden("User '%s' is not authorized to start application %s",
                    Entitlements.getEntitlementContext().user(), yaml);
        }

        try {
            return launch(yaml, spec);
        } catch (Exception e) {
            throw WebResourceUtils.badRequest(e, "Error launching blueprint");
        }
    }

    public Response launch(String yaml, EntitySpec<? extends Application> spec) {
        try {
            Application app = EntityManagementUtils.createUnstarted(mgmt(), spec);
            CreationResult<Application,Void> result = EntityManagementUtils.start(app);
            waitForStart(app, Duration.millis(100));

            boolean isEntitled = Entitlements.isEntitled(
                    mgmt().getEntitlementManager(),
                    Entitlements.INVOKE_EFFECTOR,
                    EntityAndItem.of(app, StringAndArgument.of(Startable.START.getName(), null)));

            if (!isEntitled) {
                throw WebResourceUtils.forbidden("User '%s' is not authorized to start application %s",
                        Entitlements.getEntitlementContext().user(), spec.getType());
            }

            log.info("Launched from YAML: " + yaml + " -> " + app + " (" + result.task() + ")");

            URI ref = serviceAbsoluteUriBuilder(ui.getBaseUriBuilder(), ApplicationApi.class, "get").build(app.getApplicationId());
            ResponseBuilder response = created(ref);
            if (result.task() != null)
                response.entity(TaskTransformer.fromTask(ui.getBaseUriBuilder()).apply(result.task()));
            return response.build();
        } catch (ConstraintViolationException e) {
            throw new UserFacingException(e);
        } catch (Exception e) {
            throw Exceptions.propagate(e);
        }
    }

    private void waitForStart(final Application app, Duration timeout) {
        // without this UI shows "stopped" sometimes esp if brooklyn has just started,
        // because app goes to stopped state if it sees unstarted children,
        // and that gets returned too quickly, before the start has taken effect
        Repeater.create("wait a bit for app start").every(Duration.millis(10)).limitTimeTo(timeout).until(
                new Callable<Boolean>() {
                    @Override
                    public Boolean call() throws Exception {
                        Lifecycle state = app.getAttribute(Attributes.SERVICE_STATE_ACTUAL);
                        if (state==Lifecycle.CREATED || state==Lifecycle.STOPPED) return false;
                        return true;
                    }
                }).run();
    }

    private EntitySpec<? extends Application> createEntitySpecForApplication(String potentialYaml) {
        return EntityManagementUtils.createEntitySpecForApplication(mgmt(), potentialYaml);
    }

    private String stackTraceToString(Throwable e) {
        StringBuilder sb = new StringBuilder();
        sb.append(e.getMessage()+"\n");
        sb.append(e.getClass().getName()+"\n");
        for (StackTraceElement element : e.getStackTrace()) {
            sb.append(element.toString());
            sb.append("\n");
        }
        return sb.toString();
    }
}
