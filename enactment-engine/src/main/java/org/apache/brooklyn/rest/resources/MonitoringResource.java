package org.apache.brooklyn.rest.resources;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.ws.rs.core.Response;

import org.apache.brooklyn.api.entity.Application;
import org.apache.brooklyn.api.entity.Entity;
import org.apache.brooklyn.api.entity.EntityType;
import org.apache.brooklyn.api.location.OsDetails;
import org.apache.brooklyn.api.mgmt.EntityManager;
import org.apache.brooklyn.api.sensor.AttributeSensor;
import org.apache.brooklyn.api.sensor.Sensor;
import org.apache.brooklyn.core.sensor.BasicAttributeSensor;
import org.apache.brooklyn.entity.proxy.LoadBalancer;
import org.apache.brooklyn.location.ssh.SshMachineLocation;
import org.apache.brooklyn.rest.api.MonitoringApi;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Predicates;

import eu.chorevolution.enactment.Choreography;
import eu.chorevolution.enactment.entity.ControlledDynamicChoreographyCluster;
import eu.chorevolution.enactment.entity.TomcatOdeServer;
import eu.chorevolution.monitoring.entity.IMonitorableSetOfVM;
import eu.chorevolution.monitoring.entity.IMonitorableVM;
import eu.chorevolution.monitoring.entity.MonitorableChoreographyImpl;
import eu.chorevolution.monitoring.entity.MonitorableEEImpl;
import eu.chorevolution.monitoring.entity.MonitorableVMImpl;

public class MonitoringResource extends AbstractBrooklynRestResource implements MonitoringApi {
	

	// Host name sensor name
	private final static String host_name = "host.name";
	// Host IP address sensor name
	private final static String host_address = "host.address";

	// Number of processors available to the Java virtual machine) sensor name
	private final static String processors_available = "java.metrics.processors.available";
	private final static String lb_processors_available = "processors.available";
	// The physical memory available to the operating system) sensor name
	private final static String physicalmemory_total = "java.metrics.physicalmemory.total";
	private final static String lb_physicalmemory_total = "physicalmemory.total";
	// The free memory available to the operating system)) sensor name
	private final static String physicalmemory_free = "java.metrics.physicalmemory.free";
	private final static String lb_physicalmemory_free = "physicalmemory.free";



	// average system load sensor name
	//private final static String systemload_average = "java.metrics.systemload.average";
	// Fraction of CPU time used, reported by JVM (percentage, over time window) sensor name
	private final static String processCpuTime_fraction_windowed = "java.metrics.processCpuTime.fraction.windowed";
	private final static String lb_processCpuTime_fraction_windowed = "processCpuTime.fraction.windowed";
	
	// Fraction of CPU time used, reported by JVM (percentage, last datapoint) sensor name
	//private final static String processCpuTime_fraction_last = "java.metrics.processCpuTime.fraction.last";
	
	// Uptime of Java process (millis, elapsed since start) sensor name
	//private final static String processCpuTime_total = "java.metrics.processCpuTime.total";
	// Process CPU time (total millis since start) sensor name
	//private final static String uptime= "java.metrics.uptime";
		
	// Peak number of threads sensor name
	//private final static String threads_max = "java.metrics.threads.max";
	// Current number of threads sensor name
	//private final static String threads_current = "java.metrics.threads.current";
	
	// Current local storage amount
	private final static String storage_total= "storage.total";
	// Current local storage free
	private final static String storage_free = "storage.free";

	// k size
	private final static double KB = 1024;
	// M size
	private final static double MB = KB*KB;
	// G size
	private final static double GB = MB*KB;
	// T size
	private final static double TB = GB*KB;
	
	
    private static final Logger log = LoggerFactory.getLogger(MonitoringResource.class);
	
    private IMonitorableSetOfVM getChoreographyInfo(Choreography choreography){
    	IMonitorableSetOfVM result = new MonitorableChoreographyImpl();
    	result.setKey(choreography.getId());
    	
    	Collection<IMonitorableVM> virtualMachines = getAllVirtualMachinesInfo(choreography);   
    	summarizeChoreographyInfo(result, virtualMachines);
		
		return result;
    }
    
    private List<IMonitorableVM> getAllVirtualMachinesInfo(Choreography choreography){    	
    	List<IMonitorableVM> result = new ArrayList<IMonitorableVM>();    	
		Collection<Entity> items = choreography.getChildren();
		for (Entity item: items) {
			if( item instanceof ControlledDynamicChoreographyCluster ){
				process( ((ControlledDynamicChoreographyCluster)item), result );
			}
		}	
		patchLoadBalancerSysOp(result);
		return result;
    }

    private void process(ControlledDynamicChoreographyCluster c, Collection<IMonitorableVM> vms){
    	LoadBalancer loadBalancer = c.getController();
    	vms.add(inspect(loadBalancer));
    	Collection<Entity> members = c.getMembers();
		for (Entity member: members) {
			vms.add(inspect(member));
		}
    }

    private IMonitorableVM inspect(Entity entity){
    	IMonitorableVM vm = new MonitorableVMImpl();
    	EntityType type = entity.getEntityType();    	
    	
    	setEntityInfo(entity, vm);
    	setHostInfo(entity, vm, type);
    	setMemoryInfo(entity, vm, type);
    	setCpuInfo(entity, vm, type);
    	setStorageInfo(entity, vm, type);
    	setSysOpInfo(entity, vm);
    	
    	return vm;    	
    }

	private void setEntityInfo(Entity entity, IMonitorableVM vm) {
		vm.setKey( entity.getId() );
    	vm.setChoreography( ! ( entity instanceof LoadBalancer ) );
    	vm.setLoadBalancer( entity instanceof LoadBalancer );		
		vm.setChorId(entity.getApplicationId());
		vm.setChorDisplay(entity.getApplication().getDisplayName());
	}

	private void setHostInfo(Entity entity, IMonitorableVM vm, EntityType type) {
		if( type.hasSensor(host_address) ) 
			vm.setIp((String)getValue(entity,type,host_address));
    	if( type.hasSensor(host_name) ) 
    		vm.setHostname((String)getValue(entity,type,host_name));
	}

	private void setCpuInfo(Entity entity, IMonitorableVM vm, EntityType type) {
		if( type.hasSensor(processors_available) ) 
			vm.setCpuCount(getIntValue(entity,type,processors_available));
		else if ( type.hasSensor(lb_processors_available) )
		    vm.setCpuCount(getIntValue(entity,type,lb_processors_available));
    	
		if( type.hasSensor(processCpuTime_fraction_windowed) )
    		vm.setCpuUsageRatio( getDoubleValue(entity,type,processCpuTime_fraction_windowed ));
    	else if( type.hasSensor(lb_processCpuTime_fraction_windowed) )
            vm.setCpuUsageRatio( getDoubleValue(entity,type,lb_processCpuTime_fraction_windowed ));
	}

	private void setSysOpInfo(Entity entity, IMonitorableVM vm) {
		if( ! ( entity instanceof TomcatOdeServer ) ) return;

		TomcatOdeServer tomcat = (TomcatOdeServer) entity;
        SshMachineLocation vm_machine = tomcat.getDriver().getMachine();
        OsDetails os_details = vm_machine.getOsDetails();
        vm.setSysOp(
        		(os_details.isLinux()?"linux":os_details.isMac()?"mac":os_details.isWindows()?"windows":"unknown") +";" +
        		 os_details.getName() + ";" +
        		 os_details.getVersion() + ";" + 
        		 os_details.getArch() + ";" + 
        		( os_details.is64bit() ? "64bit" : "" )
        );
	}

	private void setStorageInfo(Entity entity, IMonitorableVM vm, EntityType type) {
		if( type.hasSensor(storage_total) ) {
	    	vm.setStorageTotal( div( getIntValue(entity,type,storage_total), MB));
	    	
			if( type.hasSensor(storage_free) ) {
				long free = div( getIntValue(entity,type,storage_free), MB);
		    	vm.setStorageUsage( vm.getStorageTotal() - free );
			}
		}	
	}

	private void setMemoryInfo(Entity entity, IMonitorableVM vm, EntityType type) {
	    long totalMemory=0;
	    long freeMemory=0;

        if( type.hasSensor(physicalmemory_total) ) {
            totalMemory = getIntValue(entity,type,physicalmemory_total, MB);
            vm.setRamTotal( totalMemory );
        }
        else if( type.hasSensor(lb_physicalmemory_total) ) {
            totalMemory = getIntValue(entity,type,lb_physicalmemory_total,KB);
            vm.setRamTotal( totalMemory );
        }

        if( type.hasSensor(physicalmemory_free) ) {
            freeMemory  = getIntValue(entity,type,physicalmemory_free,MB);
        }
        else if( type.hasSensor(lb_physicalmemory_free) ) {
            freeMemory  = getIntValue(entity,type,lb_physicalmemory_free,KB);
        }

	    vm.setRamUsage( totalMemory - freeMemory );
	}
    
	private Choreography findChoreographyById(String choreographyId ){
	    EntityManager emgr = mgmt().getEntityManager();		
		Entity entity = emgr.getEntity(choreographyId);	    
		if(entity == null) return null;
		
		if (entity instanceof Application) {
			Application app = (Application) entity;
		    for (Entity appChild: app.getChildren()) {
		        if (appChild instanceof Choreography)
		            return ((Choreography)appChild);
		    }
		}
		else if (entity instanceof Choreography)
            return ((Choreography)entity);
		return null;
	}
	
	private void patchLoadBalancerSysOp(List<IMonitorableVM> vms){
		String sysOp = null;
		for( IMonitorableVM vm : vms ){
			if( sysOp != null ) break;
			sysOp = vm.getSysOp();
		}
		if( sysOp == null ) return;
		for( IMonitorableVM vm : vms ){
			if( vm.getSysOp() != null ) continue;
			vm.setSysOp(sysOp);
		}
	}
	
	private void summarizeChoreographyInfo(IMonitorableSetOfVM result, Collection<IMonitorableVM> virtualMachines) {		
		for( IMonitorableVM vm:virtualMachines ){
	    	result.setCpuUsageRatio(result.getCpuUsageRatio()+vm.getCpuUsageRatio());	
	    	result.setRamTotal(result.getRamTotal()+vm.getRamTotal());
	    	result.setRamUsage(result.getRamUsage()+vm.getRamUsage());	    	
	    	result.setStorageTotal(result.getStorageTotal()+vm.getStorageTotal());
	    	result.setStorageUsage(result.getStorageUsage()+vm.getStorageUsage());
		}
		result.setVirtualMachinesCount(virtualMachines.size());
		result.setCpuUsageRatio(result.getCpuUsageRatio()/(0.0+result.getVirtualMachinesCount()));
	}
	
	private void summarizeEEInfo(IMonitorableSetOfVM result, Collection<Entity> choreographies) {
		for (Entity choreography: choreographies){
			IMonitorableSetOfVM partial = getChoreographyInfo((Choreography)choreography);			
			result.setVirtualMachinesCount(result.getVirtualMachinesCount() + partial.getVirtualMachinesCount());
			result.setCpuUsageRatio(result.getCpuUsageRatio() + partial.getCpuUsageRatio());
			result.setRamTotal(result.getRamTotal() + partial.getRamTotal());
			result.setRamUsage(result.getRamUsage() + partial.getRamUsage());
			result.setStorageTotal(result.getStorageTotal() + partial.getStorageTotal());
			result.setStorageUsage(result.getStorageUsage() + partial.getStorageUsage());
		}
		result.setCpuUsageRatio(result.getCpuUsageRatio()/result.getVirtualMachinesCount());
	}
	
//    private String getJsonProperty(String json, String label){
//    	String raw = json.replace("{", "").replace("}", "").replace("\"", "");
//    	String[] entries = raw.split(Pattern.quote(","));
//    	for(String entry: entries){
//    		String[] kw = entry.split(Pattern.quote(":"));
//    		String k = kw[0];
//    		if( label.equalsIgnoreCase(k) && kw.length > 1 )
//    			return kw[1].trim();
//    	}
//    	return null;
//    }

    private long div( long value, double unit ){
    	return ( new Double( value / unit ) ).longValue();
    }
    
	private long getIntValue(Entity entity, EntityType type, String name, double unit){
	    //log.info("Attribute: "+name+" - Value: "+getIntValue(entity, type, name));
		return div( getIntValue(entity, type, name) , unit );
	}	
	
	private long getIntValue(Entity entity, EntityType type, String name){	
		Object obj = getValue( entity, type, name);
		if( obj == null ) return 0;
		return getIntValue(obj.toString());
	}	
	
	private long getIntValue(String value){	
		if( value == null ) return 0;
    	try{
    		return Long.parseLong(value.trim());
    	}
    	catch(Exception ex ){
    		return 0;
    	}
	}
	
	private double getDoubleValue(Entity entity, EntityType type, String name){	
    		Object obj = getValue( entity, type, name);
    		if( obj == null ) return 0d;
    		return getDoubleValue(obj.toString());
	}
		
	private double getDoubleValue(String value){	
		if( value == null ) return 0;
    	try{
    		return Double.parseDouble(value.trim());
    	}
    	catch(Exception ex ){
    		return 0d;
    	}
	}
	
	private Object getValue(Entity entity, EntityType type, String name){	
		return RestValueResolver.resolving(entity.getAttribute(findSensor(entity, type, name))).resolve();
	}

    private AttributeSensor<?> findSensor(Entity entity, EntityType type, String name) {
        Sensor<?> s = type.getSensor(name);
        if (s instanceof AttributeSensor) return (AttributeSensor<?>) s;
        return new BasicAttributeSensor<Object>(Object.class, name);
    }
	
	/* (non-Javadoc)
	 * @see eu.chorevolution.ee.monitoring.IMonitoringApi#getEngineStatus()
	 */
	@Override
	public Response getEngineStatus() {
	    IMonitorableSetOfVM result = new MonitorableEEImpl(); 
	    EntityManager emgr = mgmt().getEntityManager();		
		Collection<Entity> choreographies = emgr.findEntities(Predicates.instanceOf(Choreography.class));
		summarizeEEInfo(result, choreographies);
		
	    return Response.ok(result).build();
	}
	
	/* (non-Javadoc)
	 * @see eu.chorevolution.ee.monitoring.IMonitoringApi#getEngineVirtualMachines()
	 */
	@Override
	public Response getEngineVirtualMachines() {
		List<IMonitorableVM> result = new ArrayList<IMonitorableVM>();
	    EntityManager emgr = mgmt().getEntityManager();		
		Collection<Entity> choreographies = emgr.findEntities(Predicates.instanceOf(Choreography.class));		
		for (Entity choreography: choreographies){
			result.addAll(getAllVirtualMachinesInfo((Choreography)choreography));
		}
	    return Response.ok(result).build();
	}
	
	/* (non-Javadoc)
	 * @see eu.chorevolution.ee.monitoring.IMonitoringApi#getChorStatus(java.lang.String)
	 */
	@Override
	public Response getChorStatus(String choreographyId) {
		if (choreographyId == null || choreographyId.isEmpty()) {
	        return Response.serverError().entity("choreographyId cannot be blank").build();
	    }
		Choreography choreography = findChoreographyById(choreographyId);
		if(choreography == null) {
	        return Response.status(Response.Status.NOT_FOUND).entity("Choreography not found for choreographyId: " + choreographyId).build();
	    }		
		IMonitorableSetOfVM result = getChoreographyInfo((Choreography)choreography);	
	    return Response.ok(result).build();
	}
	
	/* (non-Javadoc)
	 * @see eu.chorevolution.ee.monitoring.IMonitoringApi#getChorVirtualMachines(java.lang.String)
	 */
	@Override
	public Response getChorVirtualMachines(String choreographyId) {
		if (choreographyId == null || choreographyId.isEmpty()) {
	        return Response.serverError().entity("choreographyId cannot be blank").build();
	    }
		Choreography choreography = findChoreographyById(choreographyId);
		if(choreography == null) {
	        return Response.status(Response.Status.NOT_FOUND).entity("Choreography not found for choreographyId: " + choreographyId).build();
	    }		
		List<IMonitorableVM> result = getAllVirtualMachinesInfo((Choreography)choreography);	
	    return Response.ok(result).build();
	}	
	
}
