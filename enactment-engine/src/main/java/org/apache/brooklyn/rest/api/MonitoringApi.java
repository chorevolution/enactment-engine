package org.apache.brooklyn.rest.api;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;


@Path("/monitoring")
@Api("Monitoring")
public interface MonitoringApi {

	/**
	 * 
	 * @return
	 * 			HTTP code 200 (OK). Body response the IMonitorableSetOfVM instance representing enactment engine's resources status
	 *          (NOT_FOUND) if enactment engine does not exist
	 */
	@GET
	@Path("/ee")
	@Produces(MediaType.APPLICATION_JSON)
	Response getEngineStatus();

	/**
	 * 
	 * @return
	 * 			HTTP code 200 (OK). Body response the IMonitorableVM[] representing resources status of enactment engine's virtual machines
	 *          (NOT_FOUND) if enactment engine does not exist
	 */
	@GET
	@Path("/ee/vm")
	@Produces(MediaType.APPLICATION_JSON)
	Response getEngineVirtualMachines();

	/**
	 * 
	 * @param choreographyId 
	 * 			the choreography id provided in the URI
	 * @return
	 * 			HTTP code 200 (OK). Body response the IMonitorableSetOfVM instance representing choreography's resources status
	 * 	 		(BAD_REQUEST) if chorId is not properly provided HTTP code 404
	 *          (NOT_FOUND) if choreography does not exist
	 */
	@GET
	@Path("/chor/{choreographyId}")
	@Produces(MediaType.APPLICATION_JSON)
	Response getChorStatus(
            @ApiParam(name = "choreographyId", value = "The ID of the choregraphy to monitor", required = true)
            @PathParam("choreographyId") String choreographyId
	);

	/**
	 * 
	 * @param choreographyId 
	 * 			the choreography id provided in the URI
	 * @return
	 * 			HTTP code 200 (OK). Body response the IMonitorableVM[] representing resources status of choreography's virtual machines
	 * 	 		(BAD_REQUEST) if chorId is not properly provided HTTP code 404
	 *          (NOT_FOUND) if choreography does not exist
	 */
	@GET
	@Path("/chor/{choreographyId}/vm")
	@Produces(MediaType.APPLICATION_JSON)
	Response getChorVirtualMachines(
            @ApiParam(name = "choreographyId", value = "The ID of the choregraphy to monitor", required = true)
            @PathParam("choreographyId") String choreographyId
	);

}