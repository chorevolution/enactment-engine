/*
 * Copyright 2016 The CHOReVOLUTION project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.chorevolution.enactment.entity;

import java.util.List;
import java.util.Map;

import org.apache.brooklyn.api.catalog.Catalog;
import org.apache.brooklyn.api.entity.ImplementedBy;
import org.apache.brooklyn.core.sensor.BasicAttributeSensorAndConfigKey;
import org.apache.brooklyn.entity.webapp.ControlledDynamicWebAppCluster;
import org.apache.brooklyn.util.core.flags.SetFromFlag;

@Catalog(name="Controlled Dynamic Choreography Cluster", description="A cluster of load-balanced web-apps implementing a Chorevolution choreography, which can be dynamically re-sized")
@ImplementedBy(ControlledDynamicChoreographyClusterImpl.class)
public interface ControlledDynamicChoreographyCluster extends ControlledDynamicWebAppCluster {


    @SuppressWarnings({ "unchecked", "rawtypes" })
    @SetFromFlag("coordination_delegates")
    public static BasicAttributeSensorAndConfigKey<Map<String, String>> CD_SPEC = new BasicAttributeSensorAndConfigKey(
            Map.class, "coordination_delegates", "Coordination delegates");
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
    @SetFromFlag("chorspec")
    public static BasicAttributeSensorAndConfigKey<List<Map<String, Map<String, String>>>> CHOR_SPEC = new BasicAttributeSensorAndConfigKey(
            List.class, "chorspec", "Choreography specifications");

    
}
