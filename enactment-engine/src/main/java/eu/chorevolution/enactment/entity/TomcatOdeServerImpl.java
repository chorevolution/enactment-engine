/*
 * Copyright 2016 The CHOReVOLUTION project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.chorevolution.enactment.entity;

import java.util.List;

import org.apache.brooklyn.entity.webapp.tomcat.TomcatServerImpl;

public class TomcatOdeServerImpl extends TomcatServerImpl implements TomcatOdeServer {
    
	@SuppressWarnings("rawtypes")
    @Override
    public Class getDriverInterface() {
        return TomcatOdeSshDriver.class;
    }
    

    public TomcatOdeSshDriver getDriver() {
        return (TomcatOdeSshDriver) super.getDriver();
    }
    
	public void deployProcess(String url) {
		// TODO Auto-generated method stub
		getDriver().deployProcess(url);
		
	}


	public void deployMultipleProcesses (List<String> urls) {
		for (String url: urls) {
			deployProcess(url);
		}
		
	}

}
