/*
 * Copyright 2016 The CHOReVOLUTION project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.chorevolution.enactment.entity;

import java.util.List;

import org.apache.brooklyn.api.catalog.Catalog;
import org.apache.brooklyn.api.entity.ImplementedBy;
import org.apache.brooklyn.core.annotation.Effector;
import org.apache.brooklyn.core.annotation.EffectorParam;
import org.apache.brooklyn.core.effector.MethodEffector;
import org.apache.brooklyn.entity.webapp.tomcat.Tomcat8Server;

/**
 * An {@link org.apache.brooklyn.api.entity.Entity} that represents a single Tomcat+ODE instance.
 */
@Catalog(name="Tomcat Server with Apache ODE",
        description="Apache Tomcat is an open source software implementation of the Java Servlet and JavaServer Pages technologies",
        iconUrl="classpath:///tomcat-logo.png")
@ImplementedBy(TomcatOdeServerImpl.class)
public interface TomcatOdeServer extends Tomcat8Server {
	MethodEffector<String> DEPLOY_PROCESS = new MethodEffector<String>(TomcatOdeServerImpl.class, "deployProcess");
	MethodEffector<String> DEPLOY_PROCESSES = new MethodEffector<String>(TomcatOdeServerImpl.class, "deployMultipleProcesses");

	@Effector(description="Deploy an ODE process")
	public void deployProcess(@EffectorParam(name="url") String url);
	
	@Effector(description="Deploy an ODE process")
	public void deployMultipleProcesses(@EffectorParam(name="urls") List<String> urls);
    
	public TomcatOdeSshDriver getDriver();
}
