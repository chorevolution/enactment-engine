/*
 * Copyright 2016 The CHOReVOLUTION project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.chorevolution.enactment.entity;

import static java.lang.String.format;

import java.util.LinkedList;
import java.util.List;

import org.apache.brooklyn.entity.webapp.tomcat.TomcatServerImpl;
import org.apache.brooklyn.entity.webapp.tomcat.TomcatSshDriver;
import org.apache.brooklyn.location.ssh.SshMachineLocation;
import org.apache.brooklyn.util.os.Os;
import org.apache.brooklyn.util.ssh.BashCommands;

public class TomcatOdeSshDriver extends TomcatSshDriver {

	public TomcatOdeSshDriver(TomcatServerImpl entity, SshMachineLocation machine) {
		super(entity, machine);
	}

	public void deployProcess(String url) {
        List<String> commands = new LinkedList<String>();
        url=url.trim();
        commands.add(BashCommands.INSTALL_CURL);
        String directory=Os.mergePaths(getRunDir(), "webapps", "ode", "WEB-INF", "processes", url.substring(url.lastIndexOf("/")+1).replace(".tar", "").replace(".gz",""));
        if (url.endsWith(".gz")) {
            commands.add(format("mkdir -p "+directory+" && curl "+url+" | tar xz -C "+directory));
        }
        else {
            commands.add(format("mkdir -p "+directory+" && curl "+url+" | tar x -C "+directory));
        }

        commands.add(format("find "+directory+" -type f -exec mv -t "+directory+" {} +"));
        newScript(CUSTOMIZING)
                .environmentVariablesReset()
                .body.append(commands)
                .execute();
	}
}
