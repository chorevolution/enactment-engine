/*
 * Copyright 2016 The CHOReVOLUTION project.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.chorevolution.enactment.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import org.apache.brooklyn.api.entity.Entity;
import org.apache.brooklyn.api.location.Location;
import org.apache.brooklyn.api.mgmt.Task;
import org.apache.brooklyn.core.effector.MethodEffector;
import org.apache.brooklyn.core.entity.Entities;
import org.apache.brooklyn.core.entity.lifecycle.Lifecycle;
import org.apache.brooklyn.core.entity.lifecycle.ServiceStateLogic;
import org.apache.brooklyn.core.entity.trait.Startable;
import org.apache.brooklyn.core.feed.ConfigToAttributes;
import org.apache.brooklyn.entity.proxy.LoadBalancer;
import org.apache.brooklyn.entity.webapp.ControlledDynamicWebAppClusterImpl;
import org.apache.brooklyn.util.collections.MutableList;
import org.apache.brooklyn.util.collections.MutableMap;
import org.apache.brooklyn.util.exceptions.Exceptions;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;

public class ControlledDynamicChoreographyClusterImpl extends ControlledDynamicWebAppClusterImpl implements ControlledDynamicChoreographyCluster {
    public static final MethodEffector<Void> DEPLOY_CD = new MethodEffector<Void>(TomcatOdeServer.class, "deployProcess");

    @Override
    public void start(Collection<? extends Location> locations) {
        ConfigToAttributes.apply(this, CD_SPEC);
        ServiceStateLogic.setExpectedState(this, Lifecycle.STARTING);
        List<Entity> childrenToStart=null;
        try {
            if (isLegacyConstruction()) {
                init();
            }

            if (locations.isEmpty()) locations = getLocations();
            addLocations(locations);

            LoadBalancer loadBalancer = getController();
            loadBalancer.bind(MutableMap.of("serverPool", getControlledGroup()));

            childrenToStart = MutableList.<Entity>of(getCluster());
            // Set controller as child of cluster, if it does not already have a parent
            if (getController().getParent() == null) {
                addChild(getController());
            }

            // And only start controller if we are parent. Favour locations defined on controller, if present
            Task<List<Void>> startControllerTask = null;
            if (this.equals(getController().getParent())) {
                if (getController().getLocations().size() == 0) {
                    childrenToStart.add(getController());
                } else {
                    startControllerTask = Entities.invokeEffectorList(this, MutableList.<Entity>of(getController()), Startable.START, ImmutableMap.of("locations", getController().getLocations()));
                }
            }

            Entities.invokeEffectorList(this, childrenToStart, Startable.START, ImmutableMap.of("locations", locations)).get();
            if (startControllerTask != null) {
                startControllerTask.get();
            }

            // wait for everything to start, then update controller, to ensure it is up to date
            // (will happen asynchronously as members come online, but we want to force it to happen)
            getController().update();
            deployProcesses(getCluster().getChildren());            

            ServiceStateLogic.setExpectedState(this, Lifecycle.RUNNING);
        } catch (Exception e) {
            ServiceStateLogic.setExpectedState(this, Lifecycle.ON_FIRE);
            throw Exceptions.propagate(e);
        } finally {
            connectSensors();
        }
    }

    public void deployProcesses(Collection<Entity> entities) throws InterruptedException, ExecutionException {
        Map<String, String> cd_specs = getConfig(CD_SPEC);
        Iterable<TomcatOdeServer> targets = Iterables.filter(entities, TomcatOdeServer.class);
        log.info("Deploying "+cd_specs+" across cluster "+this);
        Entities.invokeEffectorList(this, targets, TomcatOdeServer.DEPLOY_PROCESSES, ImmutableMap.of("urls", cd_specs.values())).get();
        log.info("CD Deployment completed");


    }

    @Override
    public Integer resize(Integer desiredSize) {
        try {
            log.info("New desired size for cluster: "+desiredSize);
            Collection<Entity> old_cluster = getCluster().getChildren();
            Integer new_size = getCluster().resize(desiredSize);
            getController().update();

            Collection<Entity> new_cluster = new ArrayList<Entity>(getCluster().getChildren());
            new_cluster.removeAll(old_cluster);
            deployProcesses(new_cluster);
            ServiceStateLogic.setExpectedState(this, Lifecycle.RUNNING);
            return new_size;
        } catch (Exception e) {
            ServiceStateLogic.setExpectedState(this, Lifecycle.ON_FIRE);
            throw Exceptions.propagate(e);
        }
    }
}
