/*
 * Copyright 2016 The CHOReVOLUTION project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eu.chorevolution.enactment;

import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;

import org.apache.brooklyn.api.location.Location;
import org.apache.brooklyn.core.entity.AbstractEntity;
import org.apache.brooklyn.core.entity.Entities;
import org.apache.brooklyn.core.entity.lifecycle.Lifecycle;
import org.apache.brooklyn.core.entity.lifecycle.ServiceStateLogic;
import org.apache.brooklyn.core.entity.lifecycle.ServiceStateLogic.ServiceNotUpLogic;
import org.apache.brooklyn.core.entity.trait.Startable;
import org.apache.brooklyn.core.location.cloud.CloudLocationConfig;
import org.apache.brooklyn.feed.function.FunctionFeed;
import org.apache.brooklyn.feed.function.FunctionPollConfig;
import org.apache.brooklyn.util.exceptions.Exceptions;
import org.apache.brooklyn.util.http.HttpTool;
import org.apache.brooklyn.util.http.HttpToolResponse;
import org.apache.http.HttpHeaders;
import org.apache.http.client.HttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.ImmutableMap;

import eu.chorevolution.chors.Base;
import eu.chorevolution.chors.BaseService;
import eu.chorevolution.datamodel.ChoreographyService;
import eu.chorevolution.datamodel.ConfigurableExistingService;
import eu.chorevolution.datamodel.DeployedService;
import eu.chorevolution.datamodel.ExistingService;
import eu.chorevolution.datamodel.PackageType;
import eu.chorevolution.datamodel.ServiceDependency;
import eu.chorevolution.datamodel.ServiceType;
import eu.chorevolution.datamodel.StatusType;
import eu.chorevolution.datamodel.deployment.DeploymentInfo;

/**
 * The entity's implementation. It is normal to extend one of the pre-existing superclasses
 * such as the most basic {@link AbstractEntity} or (for a software process being installed)
 * a {@link org.apache.brooklyn.entity.software.base.SoftwareProcessImpl}. However, for the 
 * latter it is strongly encouraged to instead consider using YAML-based blueprints.
 * 
 * The service.isUp and service.state sensors are populated by using the {@link ServiceNotUpLogic}
 * and {@link ServiceStateLogic} utilities. A simpler (less powerful) way is to directly set
 * the sensors {@link org.apache.brooklyn.core.entity.Attributes#SERVICE_UP} and
 * {@link org.apache.brooklyn.core.entity.Attributes#SERVICE_STATE_ACTUAL}. The advantage
 * of the {@link ServiceNotUpLogic} is that multiple indicators can be used, all of which
 * must be satisfied for the service to be considered healthy. For example, it might be required
 * that a web-app's URL is reachable and also that the app-server's management API reports 
 * healthy.
 */
public class ChoreographyImpl extends AbstractEntity implements Choreography {

    private static final Logger log = LoggerFactory.getLogger(ChoreographyImpl.class);
    private static final String idm_ep_notification="/%s/%s/notifyCompletion?name=%s";

    private FunctionFeed feed;

    @Override
    public void init() {
        super.init();
        //log.info("MySampleImpl.init() with config {}", config().get(MY_CONF));
        ServiceNotUpLogic.updateNotUpIndicator(this, "started", "not started");
    }

    @Override
    protected void initEnrichers() {
        super.initEnrichers();


    }

    @Override
    public void start(Collection<? extends Location> locs) {
        log.info("Choreography "+getApplicationId()+" starting");
        try {
            Entities.invokeEffectorList(this, getChildren(), Startable.START, ImmutableMap.of("locations", locs)).get();

            ServiceNotUpLogic.clearNotUpIndicator(this, "started");
            ServiceStateLogic.setExpectedState(this, Lifecycle.RUNNING);

            configureNetwork();
            eu.chorevolution.datamodel.Choreography chor = ChorSpecExtractor.getChorSpec(this);
            update_idm(chor, "CREATE");
            sensors().set(XML_CHORSPEC, chor.getXML());
        } catch (Exception e) {
            ServiceStateLogic.setExpectedState(this, Lifecycle.ON_FIRE);
            log.error(stackTraceToString(e));
            throw Exceptions.propagate(e);
        } finally {
            connectSensors();
        }
    }

    @Override
    public void stop() {
        log.info("Choreography "+getApplicationId()+" stopping");
        try {
            eu.chorevolution.datamodel.Choreography chor = ChorSpecExtractor.getChorSpec(this);
            chor.setStatus(StatusType.STOPPED);
            update_idm(chor, "DELETE");

            Entities.invokeEffectorList(this, getChildren(), Startable.STOP, ImmutableMap.of("locations",getLocations())).get();
            sensors().set(XML_CHORSPEC, "stop");
            ServiceStateLogic.setExpectedState(this, Lifecycle.STOPPED);
            ServiceNotUpLogic.updateNotUpIndicator(this, "started", "stopped");

            sensors().set(XML_CHORSPEC, chor.getXML());
        } catch (Exception e) {
            ServiceStateLogic.setExpectedState(this, Lifecycle.ON_FIRE);
            log.error(stackTraceToString(e));
            throw Exceptions.propagate(e);
        } finally {
            disconnectSensors();
        }
    }

    @Override
    public void restart() {
        log.info("Choreography "+getApplicationId()+" restarting");
        stop();
        start(getLocations());
    }

    public void configureNetwork() {
        eu.chorevolution.datamodel.Choreography chor_spec = ChorSpecExtractor.getChorSpec(this);
        Map<String, ChoreographyService> service_map=ChorSpecExtractor.create_dependencies_map(chor_spec);

        log.info("Service map: "+service_map);
        log.info("Configuring choreography "+this.getApplicationId()+" ...");
        for (ChoreographyService srv:service_map.values()) {
            List<ServiceDependency> service_deps=null;
            List <String> source_urls=new ArrayList<String>();

            String dest_name=null;
            String dest_role=null;
            String source_wsdl=null;
            String source_name=null;

            String cfg_service_name="Base";

            if (srv instanceof ConfigurableExistingService) {
                service_deps=((ConfigurableExistingService)srv).getDependencies();
                source_urls.add(((ConfigurableExistingService)srv).getUrl());
                source_wsdl=((ConfigurableExistingService)srv).getDescriptorUrl();
                log.debug("WSDL for ConfigurableExistingService: "+source_wsdl);
            }
            else if (srv instanceof DeployedService) {
                DeployedService d_srv=(DeployedService)srv;
                service_deps=d_srv.getDependencies();
                source_wsdl=d_srv.getDescriptorUrl();
                source_name=d_srv.getName();

                /* Calculate WSDL url*/
                log.debug("Initial WSDL: "+source_wsdl);

                if ( 
                        ( d_srv.getServiceType().equals(ServiceType.COORDINATION_DELEGATE) )&&
                        ( d_srv.getPackageType().equals(PackageType.ODE) )
                        )
                {
                    String cd_filename;
                    try {
                        cd_filename = Paths.get(new URI(d_srv.getPackageUrl()).getPath()).getFileName().toString().replaceAll(".tar.gz", "").replaceAll(".tgz", "");
                    } catch (URISyntaxException e) {
                        ServiceStateLogic.setExpectedState(this, Lifecycle.ON_FIRE);
                        log.error(stackTraceToString(e));
                        throw Exceptions.propagate(e);
                    }

                    source_wsdl=source_wsdl.replaceAll("ode/processes", "ode/deployment/bundles").replaceAll("\\?wsdl", "/baseService.wsdl").replaceAll(source_name, cd_filename);
                    cfg_service_name=source_name+"BaseService";
                    log.debug("WSDL for ODE package: "+source_wsdl);
                }
                else if ( d_srv.getServiceType().equals(ServiceType.BINDING_COMPONENT) ) {
                    source_wsdl=source_wsdl.replaceAll("\\?wsdl", "/BaseService?wsdl");
                    log.debug("WSDL for BC: "+source_wsdl);
                }

                /* Calculate endpoint URLs */
                for (DeploymentInfo d:d_srv.getDeploymentInfo()) {
                    if ( ( d_srv.getServiceType().equals(ServiceType.COORDINATION_DELEGATE) )&& ( d_srv.getPackageType().equals(PackageType.ODE) ) ) {
                        source_urls.add(d.getEndpoint());
                    }
                    else if ( ( d_srv.getServiceType().equals(ServiceType.SECURITY_FILTER) ) || (d_srv.getServiceType().equals(ServiceType.GLOBAL_SECURITY_FILTER) )){
                        source_urls.add(d.getEndpoint()+"/SecurityFilterManagement/setInvocationAddress");
                    }
                    else if ( d_srv.getServiceType().equals(ServiceType.BINDING_COMPONENT) ) {
                        source_urls.add(d.getEndpoint()+"/BaseService");
                        log.debug("WSDL for BC: "+source_wsdl);
                    }
                }

            }
            if (service_deps!=null) {
                for (ServiceDependency sd: service_deps) {
                    ArrayList<String> urls=new ArrayList<>();
                    dest_name=sd.getServiceSpecName();
                    dest_role=sd.getServiceSpecRole();
                    String dependency_url=null;

                    ChoreographyService d = service_map.get(dest_name);
                    if (d instanceof ExistingService) {
                        dependency_url=((ExistingService) d).getUrl();
                    }
                    else if (d instanceof ConfigurableExistingService) {
                        dependency_url=((ConfigurableExistingService) d).getUrl();
                    }
                    else if (d instanceof DeployedService) {
                        DeployedService ds = (DeployedService) d;

                        if (ds.getServiceType().equals(ServiceType.COORDINATION_DELEGATE)&& ds.getPackageType().equals(PackageType.ODE) ) {
                            String tempUrl = ds.getUrl().replace("/"+dest_name,"/"+dest_name.substring(2));
                            dependency_url=tempUrl;

                        } else {
                            dependency_url=ds.getUrl();
                        }
                    }

                    if (srv instanceof ConfigurableExistingService) {
                        urls.add(get_private_url(dependency_url));
                    }
                    else {
                        urls.add(dependency_url);
                    }
                    try {
                        for (String source_url: source_urls) {
                            log.info("[DEP] From [name:"+cfg_service_name+" - URL: "+source_url+" - WSDL: "+source_wsdl+"] TO [name:"+dest_name+" - URL: "+dependency_url+" ]");
                            BaseService service=Base.getPort(cfg_service_name, source_wsdl, source_url, BaseService.class);
                            service.setInvocationAddress(dest_role, dest_name, urls);
                        }
                    } catch (Exception e) {
                        ServiceStateLogic.setExpectedState(this, Lifecycle.ON_FIRE);
                        log.error(stackTraceToString(e));
                        throw Exceptions.propagate(e);
                    }
                }
            }
        }

        log.info("Choreography "+this.getApplicationId()+" configured");
        ServiceStateLogic.setExpectedState(this, Lifecycle.RUNNING);

    }

    public void force_idm_update(String operation){
        eu.chorevolution.datamodel.Choreography chor = ChorSpecExtractor.getChorSpec(this);
        update_idm(chor, operation);
    }


    public void update_idm(eu.chorevolution.datamodel.Choreography chor, String operation) {
        String tenant=chor.getTenant();
        if (tenant==null) 
            tenant="Master";

        for (Location l: this.getApplication().getLocations()) {
            String idm_endpoint = l.getConfig(CloudLocationConfig.IDENTITY_MANAGER_ENDPOINT);
            String idm_user=l.getConfig(CloudLocationConfig.IDENTITY_MANAGER_ENDPOINT_USERNAME);
            String idm_pass=l.getConfig(CloudLocationConfig.IDENTITY_MANAGER_ENDPOINT_PASSWORD);


            idm_endpoint="http://localhost:9080/syncope/rest/chors";
            idm_user="admin";
            idm_pass="password";

            if (idm_endpoint!=null) {
                Map<String, String> headers=new HashMap<String, String>();
                headers.put(HttpHeaders.CONTENT_TYPE, "application/xml");
                headers.put("X-Syncope-Domain", tenant);
                headers.put("Authorization", "Basic " + Base64.getEncoder().encodeToString((idm_user+":"+idm_pass).getBytes()));

                String chor_xml;
                try {
                    chor_xml = chor.getXML();
                    log.info("Sending chorspec to IDM "+idm_endpoint);
                    HttpClient http_client = HttpTool.httpClientBuilder().uri(idm_endpoint).build();

                    String idm_url=idm_endpoint+String.format(idm_ep_notification, getApplication().getId(), operation, getApplication().getDisplayName());
                    HttpToolResponse update_resp = HttpTool.httpPost(http_client,                            
                            URI.create(idm_url),
                            headers, 
                            chor_xml.getBytes());

                    log.info("IDM url: {} || response - code: {} {}", new Object[]{idm_url, Integer.toString(update_resp.getResponseCode()), update_resp.getReasonPhrase()});
                    log.info("Updated chorspec sent to IDM: "+chor_xml);
                } catch (JAXBException e) {
                    ServiceStateLogic.setExpectedState(this, Lifecycle.ON_FIRE);
                    log.error(stackTraceToString(e));
                    throw Exceptions.propagate(e);
                } catch (XMLStreamException e) {
                    ServiceStateLogic.setExpectedState(this, Lifecycle.ON_FIRE);
                    log.error(stackTraceToString(e));
                    throw Exceptions.propagate(e);
                }
            }
        }

    }

    public eu.chorevolution.datamodel.Choreography getChorSpec(){
        return ChorSpecExtractor.getChorSpec(this);
    }

    public class SpecChangeDetector implements Callable<String> {
        Choreography c=null;

        public SpecChangeDetector(Choreography chor) {
            c=chor;
        }

        @Override
        public String call() throws Exception {
            try {
                String old_chor_data=c.getAttribute(Choreography.XML_CHORSPEC);
                eu.chorevolution.datamodel.Choreography new_chor_data = ChorSpecExtractor.getChorSpec(c);
                if ((old_chor_data!=null)&&( ! (old_chor_data.equals(new_chor_data.getXML())))) {
                    configureNetwork();
                    log.info("Chorspec changed, sending updates to IDM");
                    update_idm(new_chor_data, "UPDATE");
                }
                return new_chor_data.getXML();
            }
            catch (Exception e) {
                ServiceStateLogic.setExpectedState(c, Lifecycle.ON_FIRE);
                log.error(stackTraceToString(e));
                throw Exceptions.propagate(e);
            }
        }
    }

    protected void connectSensors() {
        log.info("Adding IDM updater pointing to: "+CloudLocationConfig.IDENTITY_MANAGER_ENDPOINT);
        FunctionPollConfig<String, String> ff_config = new FunctionPollConfig<Object, String>(XML_CHORSPEC).period(1, TimeUnit.MINUTES).callable(new SpecChangeDetector(this));
        FunctionFeed ff = FunctionFeed.builder().entity(this).poll(ff_config).build();
        feed = feeds().addFeed(ff);
    }

    protected void disconnectSensors() {
        if (feed != null) feed.stop();
    }

    public String stackTraceToString(Throwable e) {
        StringBuilder sb = new StringBuilder();
        sb.append(e.getMessage()+"\n");
        sb.append(e.getClass().getName()+"\n");
        for (StackTraceElement element : e.getStackTrace()) {
            sb.append(element.toString());
            sb.append("\n");
        }
        return sb.toString();
    }

    private String get_private_url(String load_balanced_url) {
        String regex="^[^#]*?://.*?(/.*)$";
        return load_balanced_url.replaceFirst(regex, "http://localhost:8080");

    }
}
