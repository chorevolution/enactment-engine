/*
 * Copyright 2016 The CHOReVOLUTION project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.chorevolution.enactment;

import java.util.Map;

import org.apache.brooklyn.api.catalog.Catalog;
import org.apache.brooklyn.api.entity.Entity;
import org.apache.brooklyn.api.entity.ImplementedBy;
import org.apache.brooklyn.api.sensor.AttributeSensor;
import org.apache.brooklyn.core.annotation.Effector;
import org.apache.brooklyn.core.annotation.EffectorParam;
import org.apache.brooklyn.core.effector.MethodEffector;
import org.apache.brooklyn.core.entity.trait.Startable;
import org.apache.brooklyn.core.sensor.BasicAttributeSensorAndConfigKey;
import org.apache.brooklyn.core.sensor.Sensors;
import org.apache.brooklyn.util.core.flags.SetFromFlag;

/**
 * A simple entity, which demonstrates some of the things that can be done in Java when
 * implementing an entity.
 * 
 * Note the {@link ImplementedBy} annotation, which points at the class to be instantiated
 * for this entity type.
 */
@Catalog(name="Choreography", description="A CHOReVOLUTION choreography")
@ImplementedBy(ChoreographyImpl.class)
public interface Choreography extends Entity, Startable {
    
    AttributeSensor<String> XML_CHORSPEC = Sensors.newStringSensor(
            "xml_chorspec", 
            "XML specifications of the Choreography");
    
    MethodEffector<String> CONFIGURE_CHOREOGRAPHY = new MethodEffector<String>(Choreography.class, "configureNetwork");
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
    @SetFromFlag("dependencies")
    public static BasicAttributeSensorAndConfigKey<Map<String, Map<String, String>>> DEPENDENCIES = new BasicAttributeSensorAndConfigKey(
            Map.class, "dependencies", "Choreography dependencies");



    @Effector(description="Reconfigure the services composing the choreography")
    public void configureNetwork();
    
    @Effector(description="Force sending the chorspec to IDM")
    public void force_idm_update(@EffectorParam(name="operation") String operation);
    
}
