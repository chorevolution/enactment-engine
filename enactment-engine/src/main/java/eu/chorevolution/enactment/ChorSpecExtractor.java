package eu.chorevolution.enactment;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.brooklyn.api.entity.Entity;
import org.apache.brooklyn.api.location.HardwareDetails;
import org.apache.brooklyn.api.location.MachineDetails;
import org.apache.brooklyn.api.location.OsDetails;
import org.apache.brooklyn.core.entity.Attributes;
import org.apache.brooklyn.core.entity.lifecycle.Lifecycle;
import org.apache.brooklyn.core.entity.lifecycle.ServiceStateLogic;
import org.apache.brooklyn.location.ssh.SshMachineLocation;

import com.google.common.collect.Iterables;

import eu.chorevolution.datamodel.ChoreographyService;
import eu.chorevolution.datamodel.ConfigurableExistingService;
import eu.chorevolution.datamodel.DeployedService;
import eu.chorevolution.datamodel.ExistingService;
import eu.chorevolution.datamodel.PackageType;
import eu.chorevolution.datamodel.ServiceDependency;
import eu.chorevolution.datamodel.ServiceGroup;
import eu.chorevolution.datamodel.ServiceType;
import eu.chorevolution.datamodel.StatusType;
import eu.chorevolution.datamodel.deployment.CloudNode;
import eu.chorevolution.datamodel.deployment.DeploymentInfo;
import eu.chorevolution.datamodel.deployment.DeploymentLocation;
import eu.chorevolution.enactment.entity.ControlledDynamicChoreographyCluster;
import eu.chorevolution.enactment.entity.TomcatOdeServer;

public class ChorSpecExtractor {

    public static eu.chorevolution.datamodel.Choreography getChorSpec(Choreography c) {
        eu.chorevolution.datamodel.Choreography chor_data=new eu.chorevolution.datamodel.Choreography();
        StatusType status=null;
        
        
        for (Entity e:c.getChildren()) {
            if (e.getDisplayName().equalsIgnoreCase("Kibana Server")) {
                chor_data.setLogServerUrl(e.sensors().get(Attributes.MAIN_URI).toASCIIString());
            }
        }
        
        if (ServiceStateLogic.getExpectedState(c).equals(Lifecycle.RUNNING)) {
            status=StatusType.RUNNING;    
        }
        if (ServiceStateLogic.getExpectedState(c).equals(Lifecycle.STOPPED)) {
            status=StatusType.STOPPED;    
        }
        
        DeploymentLocation location=new DeploymentLocation();
        List<ServiceGroup> service_groups=new ArrayList<ServiceGroup>();
        Collection<Entity> clusters = c.getChildren();
        ControlledDynamicChoreographyCluster chor_cluster=null;
        
        List<Map<String,Map<String,String>>> cluster_spec=null;

        chor_data.setId(c.getApplicationId());
        chor_data.setLocation(location);
        chor_data.setServiceGroups(service_groups);
        chor_data.setStatus(status);

        Map<String, Map<String, String>> deps = c.getConfig(Choreography.DEPENDENCIES);

        for (Entity cluster_node: clusters) {
            ServiceGroup sg=new ServiceGroup();
            service_groups.add(sg);
            
            List<ChoreographyService> service_list=new ArrayList<ChoreographyService>();
            chor_cluster=(ControlledDynamicChoreographyCluster)cluster_node;

            String tomcat_base_url="http://"+chor_cluster.getController().getAttribute(ControlledDynamicChoreographyCluster.HOSTNAME)+":"+chor_cluster.getController().getAttribute(ControlledDynamicChoreographyCluster.HTTP_PORT)+"/";
            String ode_base_url="http://"+chor_cluster.getController().getAttribute(ControlledDynamicChoreographyCluster.HOSTNAME)+":"+chor_cluster.getController().getAttribute(ControlledDynamicChoreographyCluster.HTTP_PORT)+"/ode/processes/";
            
            cluster_spec= chor_cluster.getConfig(ControlledDynamicChoreographyCluster.CHOR_SPEC);
            for (Map<String, Map<String, String>> service_node:cluster_spec) {
                Map<String,String> service_cfg = (Map<String,String>)(service_node.get("service"));
                String service_name=(String) service_cfg.get("name");
                String service_roles=(String) service_cfg.get("roles");
                String service_type=(String) service_cfg.get("service_type");
                String service_artifact_type=(String) service_cfg.get("artifact_type");
                String service_package_type=(String) service_cfg.get("package_type");
                String service_package_url=(String) service_cfg.get("package_url");
                String service_url=(String) service_cfg.get("url");

                Map<String, String> service_deps=null;
                if (deps.containsKey(service_name)) {
                    service_deps=deps.get(service_name);
                }
                
                if (service_artifact_type.equals("existingService")) {
                    if (service_deps!=null) {
                        ConfigurableExistingService srv=null;
                        srv=new ConfigurableExistingService();
                        srv.setName(service_name);
                        srv.setRoles(Arrays.asList(service_roles));
                        srv.setUrl(service_url);
                        srv.setDependencies(create_deps(service_name, service_deps));
                        service_list.add(srv);
                    }
                    else {
                        ExistingService srv=null;
                        srv=new ExistingService();                
                        srv.setName(service_name);
                        srv.setRoles(Arrays.asList(service_roles));
                        srv.setUrl(service_url);
                        service_list.add(srv);
                    }
                }
                else if (service_artifact_type.equals("deployableService")) {
                    DeployedService srv=null;
                    srv=new DeployedService();
                    srv.setName(service_name);
                    srv.setRoles(Arrays.asList(service_roles));
                    srv.setDependencies(create_deps(service_name, service_deps));

                    srv.setPackageType(PackageType.valueOf(service_package_type));
                    srv.setPackageUrl(service_package_url);
                    srv.setServiceType(ServiceType.valueOf(service_type));

                    if (  srv.getPackageType().equals(PackageType.ODE) ) {
                        srv.setUrl(ode_base_url+srv.getName());
                        srv.setDescriptorUrl(ode_base_url+srv.getName()+"?wsdl");
                    }
                    else if ((srv.getServiceType().equals(ServiceType.SECURITY_FILTER)) ||(srv.getServiceType().equals(ServiceType.GLOBAL_SECURITY_FILTER))){
                        srv.setUrl(tomcat_base_url+srv.getName());
                        srv.setDescriptorUrl(tomcat_base_url+srv.getName()+"/"+srv.getName()+"?wsdl");
                    }
                    else {
                        srv.setUrl(tomcat_base_url+srv.getName());
                        srv.setDescriptorUrl(tomcat_base_url+srv.getName()+"?wsdl");
                    }

                    /*
                    System.out.println("WSDL for "+service_name+": "+srv.getDescriptorUrl());
                    System.out.println("ServiceType for "+service_name+": "+srv.getServiceType());
                    System.out.println("PackageType for "+service_name+": "+srv.getPackageType());
                    */
                    
                    ArrayList<DeploymentInfo> deploymentInfo = new ArrayList<DeploymentInfo>();

                    for (TomcatOdeServer e:Iterables.filter(chor_cluster.getCluster().getMembers(), TomcatOdeServer.class)) {
                        CloudNode nodeinfo;
                        DeploymentInfo d=new DeploymentInfo();
                        String srv_tomcat_base_url=null;
                        String srv_ode_base_url=null;

                        srv_tomcat_base_url="http://"+e.getAttribute(TomcatOdeServer.HOSTNAME)+":"+e.getAttribute(TomcatOdeServer.HTTP_PORT)+"/";
                        srv_ode_base_url="http://"+e.getAttribute(TomcatOdeServer.HOSTNAME)+":"+e.getAttribute(TomcatOdeServer.HTTP_PORT)+"/ode/processes/";

                        
                        if (srv.getPackageType().equals(PackageType.ODE)) {
                            d.setEndpoint(srv_ode_base_url+srv.getName());
                              
                        }
                        else {
                            if ((srv.getServiceType().equals(ServiceType.SECURITY_FILTER)) || (srv.getServiceType().equals(ServiceType.GLOBAL_SECURITY_FILTER))){
                                srv_tomcat_base_url="http://"+e.getAttribute(TomcatOdeServer.HOSTNAME)+":"+e.getAttribute(TomcatOdeServer.HTTP_PORT)+"/";
                                d.setEndpoint(srv_tomcat_base_url+srv.getName());
                            }
                            else {
                                d.setEndpoint(srv_tomcat_base_url+srv.getName());
                            }
                        }    

                        //MachineProvisioningLocation location_props = e.getAttribute(SoftwareProcess.PROVISIONING_LOCATION);


                        nodeinfo=new CloudNode();
                        if (e.getDriver()!=null) {
                            SshMachineLocation vm_machine = e.getDriver().getMachine();
                            OsDetails os_details = vm_machine.getOsDetails();
                            MachineDetails details=vm_machine.getMachineDetails();
                            if (details!=null) {
                                HardwareDetails hw_details=details.getHardwareDetails();
                                if (hw_details!=null) {
                                    nodeinfo.setCpus(hw_details.getCpuCount());
                                }
                                nodeinfo.setHostname(vm_machine.getHostname());
                                nodeinfo.setIp( (Arrays.asList(vm_machine.getPublicAddresses()).get(0)).toString() );
                                if (os_details!=null)
                                    nodeinfo.setOs(os_details.getName());
                            }

                            // TODO find hw data in brooklyn
                            /*
                        nodeinfo.setImage(image);
                        nodeinfo.setRam(hw_details.getRam());
                        nodeinfo.setPrivateKey(privateKeyFile);
                        nodeinfo.setPrivateKeyFile(privateKeyFile);
                        nodeinfo.setState(state);
                        nodeinfo.setStorage(storage);
                        nodeinfo.setUser(user);
                        nodeinfo.setZone(zone);
                             */
                        }
                        d.setNode(nodeinfo);
                        deploymentInfo.add(d);

                    }
                    srv.setDeploymentInfo(deploymentInfo);
                    srv.setInstances(chor_cluster.getCluster().getCurrentSize());
                    service_list.add(srv);
                }
                if (!service_list.isEmpty()) {
                    sg.setServices(service_list);
                }
            }
            
        }
        return chor_data;
    }
    
    private  static List<ServiceDependency> create_deps(String srv_name, Map<String, String> srv_deps) {
        String dep_name=null;
        List<ServiceDependency> sd_list=new ArrayList<ServiceDependency>();
        if (srv_deps!=null) {
            for (String dep_role:srv_deps.keySet()) {
                dep_name=srv_deps.get(dep_role);
                ServiceDependency sd=new ServiceDependency();
                sd.setServiceSpecName(dep_name);
                sd.setServiceSpecRole(dep_role);
                sd_list.add(sd);
            }
        }
        return sd_list;
    }

    public static Map<String, ChoreographyService> create_dependencies_map(eu.chorevolution.datamodel.Choreography c) {
        Map<String, ChoreographyService> res=new HashMap<String, ChoreographyService>();
        for (ServiceGroup sg:c.getServiceGroups()) {
            for (ChoreographyService srv:sg.getServices()) {
                res.put(srv.getName(), srv);
            }
        }
        return res;
    }
}

