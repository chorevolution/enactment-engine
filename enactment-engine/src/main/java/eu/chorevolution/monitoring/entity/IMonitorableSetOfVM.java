package eu.chorevolution.monitoring.entity;

public interface IMonitorableSetOfVM extends IMonitorableItem {

	/*
	 * the number of virtual machines that are used by this object
	 */
	int getVirtualMachinesCount();

	/* 
	 *  virtualMachines the number of virtual machines that are used by this object to set
	 */
	void setVirtualMachinesCount(int virtualMachines);

}