package eu.chorevolution.monitoring.entity;

/**
 * 
 * It contains information about the allocation and use of resources of a virtual machine
 * 
 * @author CEFRIEL
 *
 */
public class MonitorableVMImpl extends AbstractMonitorableItem implements IMonitorableVM  {

	private String chorId;
	private String chorDisplay;
	private String sysOp;
	private String ip;
	private String hostname;
	private long cpuCount;
	private boolean isChoreography;
	private boolean isLoadBalancer;
	
	/* (non-Javadoc)
	 * @see eu.chorevolution.ee.resources.model.IMonitorableVm#getChorId()
	 */
	@Override
	public String getChorId(){
		return chorId;
	}

	/* (non-Javadoc)
	 * @see eu.chorevolution.ee.resources.model.IMonitorableVm#setChorId(java.lang.String)
	 */
	@Override
	public void setChorId(String chorId){
		this.chorId = chorId;
	}
	
	/* (non-Javadoc)
	 * @see eu.chorevolution.ee.resources.model.IMonitorableVm#getChorDisplay()
	 */
	@Override
	public String getChorDisplay(){
		return chorDisplay;
	}
	
	/* (non-Javadoc)
	 * @see eu.chorevolution.ee.resources.model.IMonitorableVm#setChorDisplay(java.lang.String)
	 */
	@Override
	public void setChorDisplay(String chorDisplay){
		this.chorDisplay = chorDisplay;
	}
	
	/* (non-Javadoc)
	 * @see eu.chorevolution.ee.resources.model.IMonitorableVm#getSysOp()
	 */
	@Override
	public String getSysOp() {
		return sysOp;
	}
	/* (non-Javadoc)
	 * @see eu.chorevolution.ee.resources.model.IMonitorableVm#setSysOp(java.lang.String)
	 */
	@Override
	public void setSysOp(String sysOp) {
		this.sysOp = sysOp;
	}
	

	/* (non-Javadoc)
	 * @see eu.chorevolution.ee.resources.model.IMonitorableVm#getIp()
	 */
	@Override
	public String getIp() {
		return ip;
	}

	/* (non-Javadoc)
	 * @see eu.chorevolution.ee.resources.model.IMonitorableVm#setIp(java.lang.String)
	 */
	@Override
	public void setIp(String ip) {
		this.ip = ip;		
	}

	/* (non-Javadoc)
	 * @see eu.chorevolution.ee.resources.model.IMonitorableVm#getHostname()
	 */
	@Override
	public String getHostname() {
		return hostname;
	}

	/* (non-Javadoc)
	 * @see eu.chorevolution.ee.resources.model.IMonitorableVm#setHostname(java.lang.String)
	 */
	@Override
	public void setHostname(String hostname) {
		this.hostname = hostname;				
	}

	/* (non-Javadoc)
	 * @see eu.chorevolution.ee.resources.model.IMonitorableVm#getCpuCount()
	 */
	@Override
	public long getCpuCount() {
		return cpuCount;
	}
	/* (non-Javadoc)
	 * @see eu.chorevolution.ee.resources.model.IMonitorableVm#setCpuCount(int)
	 */
	@Override
	public void setCpuCount(long cpuCount) {
		this.cpuCount = cpuCount;
	}
	
	/* (non-Javadoc)
	 * @see eu.chorevolution.ee.resources.model.IMonitorableVm#isChoreography()
	 */
	@Override
	public boolean isChoreography() {
		return isChoreography;
	}
	/* (non-Javadoc)
	 * @see eu.chorevolution.ee.resources.model.IMonitorableVm#setChoreography(boolean)
	 */
	@Override
	public void setChoreography(boolean isChoreography) {
		this.isChoreography = isChoreography;
	}
	/* (non-Javadoc)
	 * @see eu.chorevolution.ee.resources.model.IMonitorableVm#isLoadBalancer()
	 */
	@Override
	public boolean isLoadBalancer() {
		return isLoadBalancer;
	}
	/* (non-Javadoc)
	 * @see eu.chorevolution.ee.resources.model.IMonitorableVm#setLoadBalancer(boolean)
	 */
	@Override
	public void setLoadBalancer(boolean isLoadBalancer) {
		this.isLoadBalancer = isLoadBalancer;
	}
}
