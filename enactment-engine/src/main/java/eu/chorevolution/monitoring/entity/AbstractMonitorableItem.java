package eu.chorevolution.monitoring.entity;

/**
 * Abstract class that provides the implementation of the common properties of the EE, VM and choreography levels
 * 
 * @author CEFRIEL
 *
 */
public class AbstractMonitorableItem implements IMonitorableItem {

	private String key;
	private double cpuUsageRatio;
	private long ramUsage;
	private long ramTotal;
	private long storageUsage;
	private long storageTotal;
	
	/* (non-Javadoc)
	 * @see eu.chorevolution.ee.resources.model.MonitorableItem#getKey()
	 */
	@Override
	public String getKey() {
		return key;
	}
	/* (non-Javadoc)
	 * @see eu.chorevolution.ee.resources.model.MonitorableItem#setKey(java.lang.String)
	 */
	@Override
	public void setKey(String key) {
		this.key = key;
	}
	/* (non-Javadoc)
	 * @see eu.chorevolution.ee.resources.model.MonitorableItem#getCpuUsageRatio()
	 */
	@Override
	public double getCpuUsageRatio() {
		return cpuUsageRatio;
	}
	/* (non-Javadoc)
	 * @see eu.chorevolution.ee.resources.model.MonitorableItem#setCpuUsageRatio(double)
	 */
	@Override
	public void setCpuUsageRatio(double cpuUsageRatio) {
		this.cpuUsageRatio = cpuUsageRatio;
	}
	/* (non-Javadoc)
	 * @see eu.chorevolution.ee.resources.model.MonitorableItem#getRamUsage()
	 */
	@Override
	public long getRamUsage() {
		return ramUsage;
	}
	/* (non-Javadoc)
	 * @see eu.chorevolution.ee.resources.model.MonitorableItem#setRamUsage(int)
	 */
	@Override
	public void setRamUsage(long ramUsage) {
		this.ramUsage = ramUsage;
	}
	/* (non-Javadoc)
	 * @see eu.chorevolution.ee.resources.model.MonitorableItem#getRamTotal()
	 */
	@Override
	public long getRamTotal() {
		return ramTotal;
	}
	/* (non-Javadoc)
	 * @see eu.chorevolution.ee.resources.model.MonitorableItem#setRamTotal(int)
	 */
	@Override
	public void setRamTotal(long ramTotal) {
		this.ramTotal = ramTotal;
	}
	/* (non-Javadoc)
	 * @see eu.chorevolution.ee.resources.model.MonitorableItem#getStorageUsage()
	 */
	@Override
	public long getStorageUsage() {
		return storageUsage;
	}
	/* (non-Javadoc)
	 * @see eu.chorevolution.ee.resources.model.MonitorableItem#setStorageUsage(int)
	 */
	@Override
	public void setStorageUsage(long storageUsage) {
		this.storageUsage = storageUsage;
	}
	/* (non-Javadoc)
	 * @see eu.chorevolution.ee.resources.model.MonitorableItem#getStorageTotal()
	 */
	@Override
	public long getStorageTotal() {
		return storageTotal;
	}
	/* (non-Javadoc)
	 * @see eu.chorevolution.ee.resources.model.MonitorableItem#setStorageTotal(int)
	 */
	@Override
	public void setStorageTotal(long storageTotal) {
		this.storageTotal = storageTotal;
	}
}
