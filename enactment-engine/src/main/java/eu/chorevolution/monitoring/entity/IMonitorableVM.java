package eu.chorevolution.monitoring.entity;

public interface IMonitorableVM extends IMonitorableItem {

	/**
	 * @return the choreography id 
	 */
	String getChorId();

	/**
	 * @param chorId the choreography to set
	 */
	void setChorId(String chorId);
	
	/**
	 * @return the choreography display name
	 */
	String getChorDisplay();

	/**
	 * @param chorDisplay the choreography display name to set
	 */
	void setChorDisplay(String chorDisplay);
	
	/**
	 * @return the operating system
	 */
	String getSysOp();

	/**
	 * @param sysOp the operating system to set
	 */
	void setSysOp(String sysOp);
	
	/**
	 * @return the ip address
	 */
	String getIp();

	/**
	 * @param ip the ip address to set
	 */
	void setIp(String ip);
	
	/**
	 * @return the hostname
	 */
	String getHostname();

	/**
	 * @param hostname the hostname to set
	 */
	void setHostname(String hostname);

	/**
	 * @return the number of cpu 
	 */
	long getCpuCount();

	/**
	 * @param cpuCount the number of cpu  to set
	 */
	void setCpuCount(long cpuCount);

	/**
	 * @return true if the virtual machine hosts a choreography
	 */
	boolean isChoreography();

	/**
	 * @param isChoreography the isChoreography to set
	 */
	void setChoreography(boolean isChoreography);

	/**
	 * @return the is a load balancer
	 */
	boolean isLoadBalancer();

	/**
	 * @param isLoadBalancer the isLoadBalancer to set
	 */
	void setLoadBalancer(boolean isLoadBalancer);

}