package eu.chorevolution.monitoring.entity;

public interface IMonitorableItem {

	/**
	 * @return the key that identify the item
	 */
	String getKey();

	/**
	 * @param key the key that identify the item
	 */
	void setKey(String key);

	/**
	 * @return the ratio of cpu usage
	 */
	double getCpuUsageRatio();

	/**
	 * @param cpuUsageRatio the ratio of cpu usage to set
	 */
	void setCpuUsageRatio(double cpuUsageRatio);

	/**
	 * @return the used quantity of memory in KB
	 */
	long getRamUsage();

	/**
	 * @param ramUsage the used quantity of memory in KBto set
	 */
	void setRamUsage(long ramUsage);

	/**
	 * @return the total quantity of memory in KB
	 */
	long getRamTotal();

	/**
	 * @param ramTotal the total quantity of memory in KB to set
	 */
	void setRamTotal(long ramTotal);

	/**
	 * @return the the used quantity of storage in MB
	 */
	long getStorageUsage();

	/**
	 * @param storageUsage the used quantity of storage in MB
	 */
	void setStorageUsage(long storageUsage);

	/**
	 * @return the total quantity of storage in MB
	 */
	long getStorageTotal();

	/**
	 * @param l the total quantity of storage in MB
	 */
	void setStorageTotal(long l);

}