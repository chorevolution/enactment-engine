package eu.chorevolution.monitoring.entity;

/**
 * 
 * It contains information about the allocation and use of resources of an enactment engine
 * 
 * @author CEFRIEL
 *
 */
public class MonitorableEEImpl extends AbstractMonitorableItem implements IMonitorableSetOfVM {
	
	private int virtualMachinesCount;
	
	/*
	 * the number of virtual machines that are used by this object
	 */
	/* (non-Javadoc)
	 * @see eu.chorevolution.ee.resources.model.IMonitorableSetOfVM#getVirtualMachinesCount()
	 */
	@Override
	public int getVirtualMachinesCount() {
		return virtualMachinesCount;
	}
	/* 
	 *  virtualMachines the number of virtual machines that are used by this object to set
	 */
	/* (non-Javadoc)
	 * @see eu.chorevolution.ee.resources.model.IMonitorableSetOfVM#setVirtualMachinesCount(int)
	 */
	@Override
	public void setVirtualMachinesCount(int virtualMachinesCount) {
		this.virtualMachinesCount = virtualMachinesCount;
	}
	
}
