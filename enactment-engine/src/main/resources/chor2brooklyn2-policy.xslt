<!--
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 
-->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="text" omit-xml-declaration="yes"/>
	<xsl:template match="/">
name: CHOREOGRAPHY_NAME

location: named:cefriel-openstack

services:

- type: org.apache.brooklyn.entity.webapp.ControlledDynamicWebAppCluster
  name: StApp_wp4
  brooklyn.config:
    wars.named: [ 
    <xsl:for-each select="choreographySpec/deployableServiceSpecs/packageUri">
      "<xsl:value-of select="."/>"<xsl:choose><xsl:when test="position() != last()">,</xsl:when></xsl:choose>
    </xsl:for-each>
    ]

    java.version.required: 1.8
    memberSpec:
      $brooklyn:entitySpec:
        type: brooklyn.entity.webapp.tomcat.Tomcat8Server
        install.version: 8.0.32
        http.port: 8080
        java.sysprops:
          file.encoding: UTF-8

  brooklyn.policies:
    - policyType: org.apache.brooklyn.policy.autoscaling.AutoScalerPolicy
      brooklyn.config:
        metric: $brooklyn:sensor("brooklyn.entity.webapp.DynamicWebAppCluster", "webapp.reqs.perSec.windowed.perNode")
        metricLowerBound: 10
        metricUpperBound: 100
        minPoolSize: 1
        maxPoolSize: 5
	</xsl:template>
</xsl:stylesheet>
