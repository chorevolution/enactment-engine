<!--
 Licensed to the Apache Software Foundation (ASF) under one
 or more contributor license agreements.  See the NOTICE file
 distributed with this work for additional information
 regarding copyright ownership.  The ASF licenses this file
 to you under the Apache License, Version 2.0 (the
 "License"); you may not use this file except in compliance
 with the License.  You may obtain a copy of the License at
 
 http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing,
 software distributed under the License is distributed on an
 "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 KIND, either express or implied.  See the License for the
 specific language governing permissions and limitations
 under the License.
 
-->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<xsl:output method="text" omit-xml-declaration="yes"/>
	<xsl:template match="/choreography">
name: CHOREOGRAPHY_NAME

location: named:chorevolution

services:
    - type: brooklyn.entity.basic.SameServerEntity
      name: Monitoring
      brooklyn.children:
        - type: org.apache.brooklyn.entity.nosql.elasticsearch.ElasticSearchNode
          id: es
          name:  Elasticsearch
          brooklyn.config:
            install.version: 1.4.4
            
        - type: kibana-standalone
          id: kibana
          name: Kibana Server
          customize.latch: $brooklyn:entity("es").attributeWhenReady("service.isUp")
          brooklyn.config:
            kibana.elasticsearch.ip: localhost
            kibana.elasticsearch.port: 9200

    - type: eu.chorevolution.enactment.Choreography
      brooklyn.children:
      <xsl:apply-templates></xsl:apply-templates>

      brooklyn.config:
        children.startable.mode: foreground
        dependencies:
        <xsl:for-each select="service_group">
            <xsl:for-each select="service[dependency]">
                <xsl:text>  </xsl:text><xsl:value-of select="name" /><xsl:text>:&#xa;            </xsl:text>
                <xsl:for-each select="dependency">
                    <xsl:value-of select="serviceSpecRole" /><xsl:text>: </xsl:text><xsl:value-of select="serviceSpecName" />
                    <xsl:choose>
                        <xsl:when test="position() != last()">
                            <xsl:text>&#xa;            </xsl:text>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:text>&#xa;        </xsl:text>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:for-each>
            </xsl:for-each>
        </xsl:for-each>
</xsl:template>

<xsl:template match="service_group">
      - type: eu.chorevolution.enactment.entity.ControlledDynamicChoreographyCluster
        name: CHOREOGRAPHY_NAME_Cluster<xsl:number/>
        brooklyn.config:
          wars.named: [<xsl:if test="service[packageType='ODE']">
            "http://download.forge.ow2.org/chorevolution/enactment-engine/ode.war",</xsl:if>
            <xsl:for-each select="service[packageType='WAR']">
                "<xsl:value-of select="normalize-space(packageUrl)"/>"<xsl:choose><xsl:when test="position() != last()">,</xsl:when></xsl:choose>
            </xsl:for-each>
          ]
          coordination_delegates:
          <xsl:for-each select="service[packageType='ODE']">
            <xsl:text>  </xsl:text><xsl:value-of select="name"/><xsl:text>: '</xsl:text><xsl:value-of select="packageUrl"/>'
          </xsl:for-each>

          chorspec: <xsl:for-each select="service">
            - service:
                name: <xsl:value-of select="name"/> 
                <xsl:if test="roles">
                roles: <xsl:value-of select="roles"/>
                </xsl:if>
                <xsl:if test="serviceType">
                service_type: <xsl:value-of select="serviceType"/>
                </xsl:if>
                <xsl:if test="@xsi:type">
                artifact_type: <xsl:value-of select="@xsi:type"/>
                </xsl:if>
                <xsl:if test="packageType">
                package_type: <xsl:value-of select="packageType"/>
                </xsl:if>
                <xsl:if test="packageUrl">
                package_url: <xsl:value-of select="packageUrl"/>
                </xsl:if>
                <xsl:if test="url">
                url: <xsl:value-of select="url"/>
                </xsl:if>
            </xsl:for-each>
            
          java.version.required: 1.8
          controllerSpec:
            $brooklyn:entitySpec:
              type: org.apache.brooklyn.entity.proxy.chorevolution.NginxController
              brooklyn.config:
                download.url: http://download.forge.ow2.org/chorevolution/enactment-engine/nginx-clojure-0.4.5.tar.gz
                install.version: 0.4.5
                archive.nameFormat: nginx-clojure-%s
                nginx.sticky: false
                #proxy.ssl.config:
                #  certificateSourceUrl: "http://ec2-34-250-100-49.eu-west-1.compute.amazonaws.com/ssl/certificate"
                #  keySourceUrl: "http://ec2-34-250-100-49.eu-west-1.compute.amazonaws.com/ssl/key"
       
              brooklyn.initializers:
                - type: org.apache.brooklyn.core.sensor.ssh.SshCommandSensor
                  brooklyn.config:
                    name: storage.total
                    period: 1m
                    command: df -xtmpfs -xdevtmpfs --sync -BK -l --output=size | awk '{if(NR>1) t+=$1; } END {print t}'
                    
                - type: org.apache.brooklyn.core.sensor.ssh.SshCommandSensor
                  brooklyn.config:
                    name: storage.free
                    period: 1m
                    command: df -xtmpfs -xdevtmpfs --sync -BK -l --output=avail | awk '{if(NR>1) a+=$1} END {print a}'

                - type: org.apache.brooklyn.core.sensor.ssh.SshCommandSensor
                  brooklyn.config:
                    name: physicalmemory.total
                    period: 1m
                    command: free | awk '{if(NR == 2 ) print $2}'
                    
                - type: org.apache.brooklyn.core.sensor.ssh.SshCommandSensor
                  brooklyn.config:
                    name: physicalmemory.free
                    period: 1m
                    command: free | awk '{if(NR == 2 ) print $4}'

                - type: org.apache.brooklyn.core.sensor.ssh.SshCommandSensor
                  brooklyn.config:
                    name: processCpuTime.fraction.windowed
                    period: 1m
                    command: top -bn1 | grep "Cpu(s)" | sed "s/.*, *\([0-9.]*\)%* id.*/\1/" |  awk '{print 100 - $1}'

                - type: org.apache.brooklyn.core.sensor.ssh.SshCommandSensor
                  brooklyn.config:
                    name: processors.available                     
                    period: 1m
                    command: grep processor /proc/cpuinfo | wc -l
       
          memberSpec:
            $brooklyn:entitySpec:
              type: eu.chorevolution.enactment.entity.TomcatOdeServer
              customize.latch: $brooklyn:entity("es").attributeWhenReady("service.isUp")
              brooklyn.children:
                - type: logstash-child
                  name: Logstash Child
                  brooklyn.config:
                      logstash.elasticsearch.host: $brooklyn:formatString("[\"%s:%s\"]",$brooklyn:entity("es").attributeWhenReady("host.address"),$brooklyn:entity("es").attributeWhenReady("http.port"))
                    
              brooklyn.config:
                children.startable.mode: background_late
                install.version: 8.0.49
                driver.httpPort: 8080
                webapp.enabledProtocols: [http, https]
                java.opts: [ "-Djava.security.egd=file:/dev/./urandom", "-Djavax.xml.accessExternalSchema=http"]
                java.sysprops:
                    file.encoding: UTF-8
                webapp.https.ssl:
                    keystorePassword: password
                    keyAlias: tomcat
                    keystoreUrl: https://github.com/brooklyncentral/brooklyn-tomcat-8-server/raw/master/sample-keystore

              brooklyn.initializers:
                - type: org.apache.brooklyn.core.sensor.ssh.SshCommandSensor
                  brooklyn.config:
                    name: storage.total
                    period: 1m
                    command: df -xtmpfs -xdevtmpfs --sync -BK -l --output=size | awk '{if(NR>1) t+=$1; } END {print t}'
                    
                - type: org.apache.brooklyn.core.sensor.ssh.SshCommandSensor
                  brooklyn.config:
                    name: storage.free
                    period: 1m
                    command: df -xtmpfs -xdevtmpfs --sync -BK -l --output=avail | awk '{if(NR>1) a+=$1} END {print a}'

</xsl:template>

</xsl:stylesheet>
