<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">

    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>org.apache.brooklyn</groupId>
        <artifactId>brooklyn-downstream-parent</artifactId>
        <version>0.9.0</version>  <!-- BROOKLYN_VERSION -->
    </parent>
    <properties>
        <maven.compiler.source>1.8</maven.compiler.source>
        <maven.compiler.target>1.8</maven.compiler.target>
    </properties>
    
    <groupId>eu.chorevolution.enactment</groupId>
    <artifactId>ee2</artifactId>
    <version>1.1.0-SNAPSHOT</version>

    <name>Choreography Enactment Engine for Apache Brooklyn</name>
    <description>
        CHOReVOLUTION Enactment Engine implemented as an Apache Brooklyn OSGi library.
    </description>

    <organization>
        <name>The CHOReVOLUTION project</name>
        <url>http://www.chorevolution.eu</url>
    </organization>

    <licenses>
        <license>
            <name>The Apache Software License, Version 2.0</name>
            <url>http://www.apache.org/licenses/LICENSE-2.0.txt</url>
            <distribution>repo</distribution>
        </license>
    </licenses>

    <issueManagement>
        <system>jira</system>
        <url>https://jira.ow2.org/browse/CRV</url>
    </issueManagement>

    <scm>
        <connection>scm:git:ssh://gitolite@tuleap.ow2.org/chorevolution/enactment-engine.git</connection>
        <developerConnection>scm:git:ssh://gitolite@tuleap.ow2.org/chorevolution/enactment-engine.git</developerConnection>
        <url>https://tuleap.ow2.org/plugins/git/chorevolution/enactment-engine</url>
    </scm>

    <repositories>
        <repository>
            <id>apache.snapshots</id>
            <name>Apache Snapshot Repository</name>
            <url>http://repository.apache.org/snapshots</url>
            <releases>
                <enabled>false</enabled>
            </releases>
            <snapshots>
                <enabled>true</enabled>
            </snapshots>
        </repository>
        <repository>
            <id>ow2-snapshots</id>
            <url>http://repository.ow2.org/nexus/content/repositories/snapshots/</url>
            <releases>
                <enabled>false</enabled>
            </releases>
            <snapshots>
                <enabled>true</enabled>
            </snapshots>
        </repository>
        <repository>
            <id>ow2-releases</id>
            <url>http://repository.ow2.org/nexus/content/repositories/releases/</url>
            <releases>
                <enabled>false</enabled>
            </releases>
            <snapshots>
                <enabled>true</enabled>
            </snapshots>
        </repository>
    </repositories>

    <dependencies>
        <dependency>
            <!-- All Brooklyn dependencies (note that the OSGi imports in the built artifacts will only list the packages actually used). It is possible to be far more selective about the required dependencies. -->
            <groupId>org.apache.brooklyn</groupId>
            <artifactId>brooklyn-all</artifactId>
            </dependency>

        <dependency>
            <!-- this gives us flexible and easy-to-use logging; just edit logback-custom.xml! -->
            <groupId>org.apache.brooklyn</groupId>
            <artifactId>brooklyn-logback-xml</artifactId>
            <version>${brooklyn.version}</version>
            <optional>true</optional>
            <scope>provided</scope>
        </dependency>

        <dependency>
            <!-- includes testng and useful logging for tests -->
            <groupId>org.apache.brooklyn</groupId>
            <artifactId>brooklyn-test-support</artifactId>
            <version>${brooklyn.version}</version>
            <scope>test</scope>
        </dependency>

        <dependency>
            <!-- includes org.apache.brooklyn.test.support.LoggingVerboseReporter -->
            <groupId>org.apache.brooklyn</groupId>
            <artifactId>brooklyn-utils-test-support</artifactId>
            <version>${brooklyn.version}</version>
            <scope>test</scope>
        </dependency>

        <dependency>
            <!-- for useful entity testing, such as TestEntity and BrooklynAppUnitTestSupport -->
            <groupId>org.apache.brooklyn</groupId>
            <artifactId>brooklyn-core</artifactId>
            <version>${brooklyn.version}</version>
            <classifier>tests</classifier>
            <scope>test</scope>
        </dependency>

        <!-- for useful YAML-based testing, such as AbstractYamlTest -->
        <dependency>
            <groupId>org.apache.brooklyn</groupId>
            <artifactId>brooklyn-camp</artifactId>
            <version>${brooklyn.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.apache.brooklyn</groupId>
            <artifactId>brooklyn-camp</artifactId>
            <version>${brooklyn.version}</version>
            <classifier>tests</classifier>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.apache.brooklyn</groupId>
            <artifactId>brooklyn-test-framework</artifactId>
            <version>${brooklyn.version}</version>
            <scope>test</scope>
        </dependency>

        <dependency>
        	<groupId>eu.chorevolution.ee</groupId>
        	<artifactId>chorspec</artifactId>
        	<version>1.0.1-SNAPSHOT</version>
        </dependency>
        
        <dependency>
            <groupId>org.apache.jclouds.labs</groupId>
            <artifactId>openstack-neutron</artifactId>
            <version>${jclouds.version}</version>
        </dependency>
    </dependencies>

    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <configuration>
                    <source>1.8</source>
                    <target>1.8</target>
                    <encoding>UTF-8</encoding>
                </configuration>
            </plugin>

            <plugin>
                <groupId>org.apache.rat</groupId>
                <artifactId>apache-rat-plugin</artifactId>
                <configuration>
                    <excludes>
                        <exclude>nbactions.xml</exclude>
                        <exclude>nb-configuration.xml</exclude>
                        <exclude>**/META-INF/cxf/**</exclude>
                        <exclude>**/META-INF/services/**</exclude>
                        <exclude>**/META-INF/MANIFEST.MF</exclude>            
                        <exclude>**/rat.txt</exclude>
                        <exclude>**/build-copy-javadoc-files.xml</exclude>
                        <exclude>**/maven-eclipse.xml</exclude>
                        <exclude>**/*.iml</exclude>
                        <exclude>**/*.log</exclude>
                        <exclude>.externalToolBuilders/**</exclude>
                        <exclude>.git/**</exclude>
                        <exclude>.idea/**</exclude>
                        <exclude>**/.*</exclude>
                    </excludes>
                </configuration>
                <executions>
                    <execution>
                        <id>rat-check</id>
                        <phase>verify</phase>
                        <goals>
                            <goal>check</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-plugin</artifactId>
                <configuration>
                    <redirectTestOutputToFile>true</redirectTestOutputToFile>
                    <encoding>utf-8</encoding>
                    <runOrder>alphabetical</runOrder>
                </configuration>
            </plugin>

            <!-- Put NOTICE and LICENSE files in all artifacts and javadocs -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-resources-plugin</artifactId>
                <executions>
                    <execution>
                        <id>copy-artifact-legal-files</id>
                        <phase>process-resources</phase>
                        <goals>
                            <goal>copy-resources</goal>
                        </goals>
                        <configuration>
                            <outputDirectory>${project.build.directory}/classes/META-INF</outputDirectory>
                            <resources>
                                <resource>
                                    <directory>${basedir}</directory>
                                    <includes>
                                        <include>LICENSE</include>
                                        <include>NOTICE</include>
                                    </includes>
                                </resource>
                            </resources>
                        </configuration>
                    </execution>
                    <execution>
                        <id>copy-javadoc-legal-files</id>
                        <phase>process-resources</phase>
                        <goals>
                            <goal>copy-resources</goal>
                        </goals>
                        <configuration>
                            <outputDirectory>${project.build.directory}/apidocs/META-INF</outputDirectory>
                            <resources>
                                <resource>
                                    <directory>${basedir}</directory>
                                    <includes>
                                        <include>LICENSE</include>
                                        <include>NOTICE</include>
                                    </includes>
                                </resource>
                            </resources>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            

        </plugins>
    </build>

</project>
